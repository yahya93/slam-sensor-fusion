
/*! \cite Molloy, D. [DerekMolloyDCU]. (2012, May, 3). Beaglebone: GPIO Programming on ARM Embedded Linux [Video file].
* 	Retrieved from http://www.youtube.com/watch?v=SaIpz00lE84
* 	repository source: https://github.com/derekmolloy/boneDeviceTree/tree/master/gpio
*/
/* resources: 
* http://elinux.org/Jetson/GPIO 
* http://elinux.org/Jetson/Tutorials/GPIO
* http://git.toradex.com/cgit/linux-toradex.git/tree/Documentation/gpio.txt
*/
#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <poll.h>

namespace sf{namespace jetson{ namespace gpio{

	enum PIN_DIRECTION{
		INPUT_PIN=0,
		OUTPUT_PIN=1
	};
	enum PIN_VALUE{
		LOW=0,
		HIGH=1
	};

	// event type
	#define JETSON_GPIO_INTERRUPT_RISING "rising"
	#define JETSON_GPIO_INTERRUPT_FALLING "falling"
	#define JETSON_GPIO_INTERRUPT_BOTH "both"
	#define JETSON_GPIO_INTERRUPT_NONE "none"

	int gpio_export(unsigned int gpio);
	int gpio_unexport(unsigned int gpio);
	int gpio_set_dir(unsigned int gpio, PIN_DIRECTION out_flag);
	int gpio_set_value(unsigned int gpio, PIN_VALUE value);
	int gpio_get_value(unsigned int gpio, unsigned int *value);
	int gpio_set_edge(unsigned int gpio, const char *edge);
	int gpio_fd_open(unsigned int gpio);
	int gpio_fd_close(int fd);	

	class JetsonGPIO_Interrupt_handler
	{
	private:
		struct pollfd fdset[1]; // to allocate itself
		int gpio_fd;
		int timeout;
		char buffer[64];
	public:
		void createEvent(unsigned int  in_gpio,const char* in_event,int in_event_timeout_ms);
		int waitForEvent();
		void closeEvent();

	};

}}}