#include "../../../config/config.h"
#ifdef INCLUDE_ODOMETRY
#include "jetson_gpio.h"
#pragma GCC diagnostic ignored "-Wunused-result" // due to the return value of the read()/write()

#define SYSFS_GPIO_DIR "/sys/class/gpio"
#define MAX_BUF 64

namespace sf {
    namespace jetson {
        namespace gpio {

            /****************************************************************
             * gpio_export
             ****************************************************************/
            int gpio_export(unsigned int gpio) {
                int fd, len;
                char buf[MAX_BUF];

                fd = open(SYSFS_GPIO_DIR "/export", O_WRONLY);
                if (fd < 0) {
                    perror("gpio/export: Have you run me as a sudo ?");
                    return fd;
                }

                len = snprintf(buf, sizeof (buf), "%d", gpio);
                write(fd, buf, len);
                close(fd);

                return 0;
            }

            /****************************************************************
             * gpio_unexport
             ****************************************************************/
            int gpio_unexport(unsigned int gpio) {
                int fd, len;
                char buf[MAX_BUF];

                fd = open(SYSFS_GPIO_DIR "/unexport", O_WRONLY);
                if (fd < 0) {
                    perror("gpio/unexport: Have you exported me before ?\n");
                    return fd;
                }

                len = snprintf(buf, sizeof (buf), "%d", gpio);
                write(fd, buf, len);
                close(fd);
                return 0;
            }

            /****************************************************************
             * gpio_set_dir
             ****************************************************************/
            int gpio_set_dir(unsigned int gpio, PIN_DIRECTION out_flag) {
                int fd;
                char buf[MAX_BUF];

                snprintf(buf, sizeof (buf), SYSFS_GPIO_DIR "/gpio%d/direction", gpio);

                fd = open(buf, O_WRONLY);
                if (fd < 0) {
                    perror("gpio/direction");
                    return fd;
                }

                if (out_flag == OUTPUT_PIN)
                    write(fd, "out", 4); // 4 = 3 chars + null
                else
                    write(fd, "in", 3);

                close(fd);
                return 0;
            }

            /****************************************************************
             * gpio_set_value
             ****************************************************************/
            int gpio_set_value(unsigned int gpio, PIN_VALUE value) {
                int fd;
                char buf[MAX_BUF];

                snprintf(buf, sizeof (buf), SYSFS_GPIO_DIR "/gpio%d/value", gpio);

                fd = open(buf, O_WRONLY);
                if (fd < 0) {
                    perror("gpio/set-value");
                    return fd;
                }

                if (value == LOW)
                    write(fd, "0", 2);
                else
                    write(fd, "1", 2);

                close(fd);
                return 0;
            }

            /****************************************************************
             * gpio_get_value
             ****************************************************************/
            int gpio_get_value(unsigned int gpio, unsigned int *value) {
                int fd;
                char buf[MAX_BUF];
                char ch;

                snprintf(buf, sizeof (buf), SYSFS_GPIO_DIR "/gpio%d/value", gpio);

                fd = open(buf, O_RDONLY);
                if (fd < 0) {
                    perror("gpio/get-value");
                    return fd;
                }

                read(fd, &ch, 1);

                if (ch != '0') {
                    *value = 1;
                } else {
                    *value = 0;
                }

                close(fd);
                return 0;
            }

            /****************************************************************
             * gpio_set_edge
             ****************************************************************/

            int gpio_set_edge(unsigned int gpio, const char *edge) {
                int fd;
                char buf[MAX_BUF];

                snprintf(buf, sizeof (buf), SYSFS_GPIO_DIR "/gpio%d/edge", gpio);

                fd = open(buf, O_WRONLY);
                if (fd < 0) {
                    perror("gpio/set-edge");
                    return fd;
                }

                write(fd, edge, strlen(edge) + 1);
                close(fd);
                return 0;
            }

            /****************************************************************
             * gpio_fd_open
             ****************************************************************/

            int gpio_fd_open(unsigned int gpio) {
                int fd;
                char buf[MAX_BUF];

                snprintf(buf, sizeof (buf), SYSFS_GPIO_DIR "/gpio%d/value", gpio);

                fd = open(buf, O_RDONLY | O_NONBLOCK);
                if (fd < 0) {
                    perror("gpio/fd_open");
                }
                return fd;
            }

            /****************************************************************
             * gpio_fd_close
             ****************************************************************/

            int gpio_fd_close(int fd) {
                return close(fd);
            }

            void JetsonGPIO_Interrupt_handler::createEvent(unsigned int in_gpio, const char* in_event, int in_event_timeout_ms) {
                gpio_set_edge(in_gpio, in_event);
                gpio_fd = gpio_fd_open(in_gpio);
                timeout = in_event_timeout_ms;
                memset((void*) fdset, 0, sizeof (fdset));
                fdset[0].fd = gpio_fd;
                fdset[0].events = POLLPRI;
                read(fdset[0].fd, buffer, 64); // to read any unintentional event in the begin from the system.
            }

            int JetsonGPIO_Interrupt_handler::waitForEvent() {
                fdset[0].revents = 0x00;
                int eventresult = poll(fdset, 1, timeout);

                if (fdset->revents & POLLPRI) {
                    read(fdset->fd, buffer, 64); // you must read ,otherwise it will event on the previous event.
                    return 1;
                } else
                    return eventresult; // 0 means timeout , negative means something wrong happened.
            }

            void JetsonGPIO_Interrupt_handler::closeEvent() {
                gpio_fd_close(gpio_fd);
            }

        }
    }
}
#endif