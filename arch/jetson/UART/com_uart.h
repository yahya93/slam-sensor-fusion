#ifndef UART_COMM
#define UART_COMM
// by ibrahim
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
#include <sys/ioctl.h>



void uart_init(void);
void uart_write(const char * message, int str_size);
void uart_read(char* message, size_t charCountsToRead);

// Usage example
    // uart_init();
    // char message[256] = {0};
    // std::string msg;
    // while (1) {
    //     uart_read(message, 256);
    //     msg.assign(message);
    //     std::cout << msg << "\n";
    //     sleep(1);
    // }

#endif