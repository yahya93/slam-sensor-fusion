/* www.chrisheydrick.com
     
   June 23 2012
   CanonicalArduinoRead write a byte to an Arduino, and then
   receives a serially transmitted string in response.
   The call/response Arduino sketch is here:
   https://gist.github.com/2980344
   Arduino sketch details at www.chrisheydrick.com
 */
#include "com_uart.h"
int fd, n, i;
char buf[64] = "temp text";
struct termios toptions;

void uart_init(void) {

    /* open serial port */
    fd = open("/dev/ttyACM1", O_RDWR | O_NOCTTY);
    printf("fd opened as %i\n", fd);

    /* wait for the Arduino to reboot */
    //usleep(3500000); ?????????

    /* get current serial port settings */
    tcgetattr(fd, &toptions);
    /* set 9600 baud both ways */
    cfsetispeed(&toptions, B9600);
    cfsetospeed(&toptions, B9600);
    /* 8 bits, no parity, no stop bits */
    toptions.c_cflag &= ~PARENB;
    //toptions.c_cflag &= ~CSTOPB;
    toptions.c_cflag &= ~CSIZE;
    toptions.c_cflag |= CS8;
    /* Canonical mode */
    toptions.c_lflag |= ICANON;
    /* commit the serial port settings */
    tcsetattr(fd, TCSANOW, &toptions);
}

void uart_write(const char * message, int str_size) {
#pragma GCC diagnostic ignored "-Wunused-result"
    write(fd, message, str_size);
}

void uart_read(char* message, size_t charCountsToRead) {
#pragma GCC diagnostic ignored "-Wunused-result"
    read(fd, (void*) message, charCountsToRead);
}