/*
 * main_test_1.cpp
 *
 *  Created on: Oct 16, 2016
 *      Author: yahia
 */

#include "../vision/vision.h"


#ifdef INCLUDE_ODOMETRY
using namespace sf::jetson::gpio;
#endif

int main(int argc, char**argv) {
 
     sf::vision::VisionWork vx;
     vx.vision_main(argc,argv);

    return 0;
}

