 /** 
 *  @file    vision.h
 *  @author  Yahia Farghaly
 *  @date    1/26/2017  
 *  @version 1.0 
 *  
 *  @brief Wrapper interface for computer vision tasks
 *
 */
#pragma once

#include "../main/system_includes.h"
#include "../config/zed_config.h"
#include "../config/config.h"
#include "../config/vision_config.h"
#include "../main/timer.h"
//#include "../odometry/odometry.h"
#include "camera.h"
#include "types_definition.h"
#include "features.h"
#include "id_assignment_manager.h"

namespace sf {
    namespace vision {

        /**
         *  A VisionWork class. A wrapper class for the requirement vision
         *  output.
         */
        class VisionWork {
        public:
            /*! \brief 		main of the vision layer work/
             *  \Param          None
             *  \return         output the requirements to fusion team.
             *  \Note           Should be put on another thread.
             */
            void vision_main(int argc, char**argv);

            VisionWork();
            ~VisionWork();
        private:
            float __upBoundary, __bottomBoundary;
            cv::Point __upBoundary_pix, __bottomBoundary_pix;
            bool __isBoundaryValid = false;
        protected:
            bool __frameReadSuccess;
            bool __firstTime;

            //CPU 
            cv::Mat __cpu_currentFrame;
            cv::Mat __cpu_previousFrame;
            cv::Mat __cpu_descriptors;
            
            cv::Mat __mask;
            
            std::vector< cv::KeyPoint> __cpu_current_keypoints;
            std::vector< cv::KeyPoint> __cpu_previous_keypoints;
#if defined ENABLE_DRAWING_THE_OUTPUT_RESULT
            std::vector< cv::KeyPoint> __cpu_current_keypoints_valid_depth;
#endif
            cv::Mat __finalOutputFrame;

            //GPU
            cv_gpu_mat __gpu_currentFrame;
            //cv_gpu_mat __gpu_current_keypoints; //incompatible with OpenCV3            
            //cv_gpu_mat __gpu_previous_keypoints;//incompatible with OpenCV3
 
            std::vector<cv::KeyPoint>  __gpu_current_keypoints;             
            std::vector<cv::KeyPoint>  __gpu_previous_keypoints;
            
            cv_gpu_mat __gpu_previous_descriptors;
            cv_gpu_mat __gpu_current_descriptors;

            Features __cameraEye;
            std::vector< cv::DMatch> __good_matches;
            ZEDCamera __zed;
            std::vector<KeyPoint_properties> __keyPointsList;
            IDAssignManager __idManager;


#if(USE_VISUAL_ODOMETRY)

            class visual_odometry_s {
            public:
                float yaw; // in rad by default
                float tx, tz;
                float r32, r33;
                float delta_tz, delta_tx, delta_yaw;
                float old_tx, old_tz, old_yaw;
            } visual_odm;
#endif
            unsigned int __motor1_Tick, __motor2_Tick;
            float __yaw_value;
            // for logging
#if defined ENABLE_DRAWING_THE_OUTPUT_RESULT_IN_FILE && defined ENABLE_DRAWING_THE_OUTPUT_RESULT
            cv::VideoWriter __writer;
            const char* __fileName = "./camera_record.avi";
#endif

            // internal method
            /*! \brief 		Calculating depth and angle of best matched features
             *                  and fill __the keyPointsList.
             *  \Note           the depth and angle is calculated w.r.t current
             *                  frame.
             */
            void assignFeaturesProperities();
            /*! \brief  
             *  1- Omit features which are not able to get its depth.
             *  2- Omit features which are on the floor.
             *  3- Assign IDs to the remaining features.
             *  4- Assign ranging and bearing to the remaining features.
             */
            void fillkeyPointsList(bool isFirstTime);
            /*! \brief setting the boundaries values to a fixed values*/
            void startCalibratingEnvironmentForDeterminigBoundariesValues();
            /*! \brief reduce the features density for the SLAM
             *   check configuration in config/vision_config.h
             *   TO BE CALLED AFTER fillkeyPointsList()*/
            void reduceFeaturesIntensityForSLAM();

#ifdef PRINT_SENSOR_DATA_FILE
            void generateSensorDataFile();
#endif

#ifdef ENABLE_DRAWING_THE_OUTPUT_RESULT
            const char * res_currentFrameWindowName = "Current Frame";

            typedef class drawing_s {
            public:
                unsigned int res_frameNumber = 0;
                std::string res_frameNumber_str = "0";
                std::vector< cv::KeyPoint> current_kp, currentkp_valid_depth;
                std::string res_no_current_kps_str;
                std::string res_no_valid_depth_kps_str;
                std::string res_no_matched_kps_str;
                std::string res_processing_time_str;
                float processing_time_dummy_var;
                cv::Mat outputFrame;
            } drawing_s;
            std::queue<drawing_s> drawing_queue;
            drawing_s dummy_drawing;
            sf::Timer res_processing_time;
            std::thread drawingThread;
            void drawResult();
#endif

#ifdef ENABLE_REPORT_LOG_FILE
            FILE * __reportFilePointer;
#define PRINT_REPORT 1
#else
            FILE * __reportFilePointer;
#define PRINT_REPORT 0
#endif
#define REPORT(TXT) { if(PRINT_REPORT) fprintf(__reportFilePointer,TXT); }
        };


    }
}