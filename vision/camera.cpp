
#include "camera.h"

using namespace sf::vision;
using namespace sl::zed;
using namespace std;

int WINDOW_WIDTH;
int WINDOW_HEIGHT;

ZEDCamera::ZEDCamera() {

#if (ZED_MODE == 1)
    __parameters.mode = MODE::QUALITY;
#else
    __parameters.mode = MODE::PERFORMANCE;
#endif
    __parameters.coordinate = COORDINATE_SYSTEM::IMAGE;
    __parameters.verbose = true;
#if (ZED_UNIT==0)
    __parameters.unit = UNIT::MILLIMETER;
#if ZED_FAREST_CAPTURED_DEPTH > 20000  
#error "ZED_FAREST_CAPTURED_DEPTH cannot be set to be larger than 20 meter"
#endif
#elif (ZED_UNIT==1)
    __parameters.unit = UNIT::METER;
#if ZED_FAREST_CAPTURED_DEPTH > 20  
#error "ZED_FAREST_CAPTURED_DEPTH cannot be set to be larger than 20 meter"
#endif
#elif (ZED_UNIT==2)
#if ZED_FAREST_CAPTURED_DEPTH > 2000  
#error "ZED_FAREST_CAPTURED_DEPTH cannot be set to be larger than 20 meter"
#endif
    __parameters.unit = UNIT::MILLIMETER;
#else 
#error "ZED: undefined unit is configured"
#endif
#ifdef DEVELOP_FILTERING_DYNAMIC_FEATURES
    __cameraPosition.setIdentity(4, 4);
#endif

}

ZEDCamera::~ZEDCamera() {
    delete zed_obj;
}

int ZEDCamera::initialize(int argc, char**argv) {

    if (argc > 1) {
        std::cout << "\nVision Source: SVO File\n";
        zed_obj = new Camera(argv[1]);
    } else {
        std::cout << "\nVision Source: Live Camera\n";
#if (ZED_QUALITY==0)
        zed_obj = new Camera(VGA);
#else
        zed_obj = new Camera(HD720);
#endif

    }

    ERRCODE err = zed_obj->init(__parameters);
    if (err != SUCCESS) {
        delete zed_obj;
        LOG_MSG("ZED initialization status: FAILED");
        return 1;
    }
#if (ZED_UNIT==2)
    zed_obj->setDepthClampValue(ZED_FAREST_CAPTURED_DEPTH * 10); // as cm is not exist by default in ZED
#else
    zed_obj->setDepthClampValue(ZED_FAREST_CAPTURED_DEPTH);
#endif

#ifdef DEVELOP_FILTERING_DYNAMIC_FEATURES
    if (zed_obj->enableTracking(__cameraPosition) == false) {
        LOG_MSG("ZED the area database file wasn't found: FAILED");
        return 1;
    };
#endif

    WINDOW_WIDTH = zed_obj->getImageSize().width;
    WINDOW_HEIGHT = zed_obj->getImageSize().height;
    this->height = WINDOW_HEIGHT;
    this->width = WINDOW_WIDTH;

#ifdef SHOW_CONFIDENCE_MATRIX
    this->confidenceDisp.create(WINDOW_HEIGHT, WINDOW_WIDTH, CV_8UC4);
#endif

    LOG_MSG(std::string("ZED initialization status: " + sl::zed::errcode2str(err)).c_str());

    return 0;
}

void ZEDCamera::get_dimensions(int _row, int _col, Pixel& pix) {
    float dist;

#ifdef GET_DEPTH_FROM_DEPTH_MAT
    float* ptr_image_num = (float*) ((int8_t*) __current_mDepth.data + _row * __current_mDepth.step);
    dist = ptr_image_num[_col];
#else //FROM XYZ MAT
    dist = ((float*) ((uint8_t*) __current_mXYZmat.data + _row * __current_mXYZmat.step))[4 * _col + 2];
#endif

    if (!isValidMeasure(dist)) {
        pix._x = -1;
        pix._y = -1;
        pix._z = -1;
    } else {
        pix._x = (((float*) ((uint8_t*) __current_mXYZmat.data + _row * __current_mXYZmat.step))[4 * _col]) - ZED_X_AXIS_OFFEST;
        pix._y = (((float*) ((uint8_t*) __current_mXYZmat.data + _row * __current_mXYZmat.step))[4 * _col + 1]) - ZED_Y_AXIS_OFFEST;
        pix._z = dist - ZED_Z_AXIS_OFFEST;
#if (ZED_UNIT==2)
        pix._x = pix._x / 10;
        pix._y = pix._y / 10;
        pix._z = pix._z / 10;
#endif
    }
}

#ifdef DEVELOP_FILTERING_DYNAMIC_FEATURES

void ZEDCamera::getNewPosition(Eigen::Vector3f& out_diff_pos, sl::zed::TRACKING_STATE& out_t) {
    // POSE: The matrix contains the displacement from the previous camera position to the current one
    out_t = zed_obj->getPosition(__cameraPosition, MAT_TRACKING_TYPE::POSE);
    switch (out_t) {
        case TRACKING_GOOD:
        {
            // confidence metric of the tracking [0-100], 0 means that the tracking is lost.
            printf("Tracking Confidence: %d\n", zed_obj->getTrackingConfidence());
            out_diff_pos = __cameraPosition.block(0, 3, 3, 1);
        }
            break;
        case TRACKING_LOST:
            LOG_MSG("Tracking is lost\n");
            zed_obj->enableTracking(__cameraPosition);
            break;
            break;
        default:
            break;
    }
}
#endif

void ZEDCamera::ZED_main_test() {
    //sl::zed::MotionPoseData framePosition;
    //sl::zed::TRACKING_STATE out_t = zed_obj->getPosition(framePosition, MAT_TRACKING_TYPE::POSE);

}

void ZEDCamera::getZEDCalibrationsParams(float& f, float& cx, float& cy) {
    f = zed_obj->getParameters()->LeftCam.fx;
    cx = zed_obj->getParameters()->LeftCam.cx;
    cy = zed_obj->getParameters()->LeftCam.cy;
}
