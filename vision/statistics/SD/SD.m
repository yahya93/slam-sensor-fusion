function [out_SD] = SD(in_data_vector,n_reading) 
mean = sum(in_data_vector)./n_reading;
out_SD=sqrt(sum((in_data_vector-mean).^2)/n_reading);
end