%RATIO =0.6
clc,clear all,close all;
N_frames = 20;
matched_vect_vga=[0,62.162163,4.680851,56.250000,40,78.000000,86,84,...
    76,66,78,70,88,80,82,76,86,82,70,74];
matched_vect_hd=[0,54,46,60,56,54,46,50,56,56,60,52,52,62,62,54,62,58,60,...
    68];
figure
plot(1:1:N_frames,matched_vect_vga);
hold on
plot(1:1:N_frames,matched_vect_hd);
legend('VGA Quality','HD Quality');
xlabel('Frame Number')
ylabel('Matched precentage of previous frame with current frame');
hold off

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
N_frames = 100;
matched_vect_vga=[0,46,64,70,54,78,82,84,76,76,80,84,86,82,84,80,74,82,82,76,80,76,78,78,80,76,82,82,78,88,82,72,80,82,72,80,76,74,76,82,84,80,82,82,78,80,84,80,82,78,68,86,84,78,78,76,82,78,76,74,82,82,80,72,76,74,84,82,82,78,84,72,78,88,88,80,88,78,80,78,82,86,86,74,86,80,86,82,84,74,82,80,80,82,78,84,80,88,88,82];
matched_vect_hd=[0,74,68,66,74,72,74,74,70,70,70,78,74,76,74,70,72,76,74,76,80,74,68,68,68,66,66,64,64,60,78,74,76,74,80,68,76,68,74,78,66,70,76,78,74,84,74,74,68,76,78,66,66,66,72,68,74,76,74,66,76,82,66,66,72,74,72,74,66,78,78,70,78,68,68,70,72,66,62,64,68,78,72,68,64,66,76,72,72,74,68,72,80,80,76,74,74,82,74,72];
figure
plot(1:1:N_frames,matched_vect_vga);
hold on
plot(1:1:N_frames,matched_vect_hd);
legend('VGA Quality','HD Quality');
xlabel('Frame Number')
ylabel('Matched precentage of previous frame with current frame');
hold off