/** 
 *  @file    features.h
 *  @author  Yahia Farghaly, Yahya Ahmed
 *  @date    3/25/2016
 *  @version 1.0 
 *
 *  @brief Wrappered class to manage the extraction and matching of features on both CPU and GPU.
 *
 */

#include "features.h"

using namespace sf::vision;
using namespace sl::zed;
using namespace std;

/** 
 *   @brief  Initialize __orb pointer of GPU
 */
Features::Features() {
#if MAX_EXTRACTED_FEATURES < 50 && USE_ORB_GPU
#error "NOT ALLOWED TO SET (MAX_EXTRACTED_FEATURES) less than 50 for ORB::GPU"
#endif
#if (CV_MAJOR_VERSION == 2)
    __orb = new cv::gpu::ORB_GPU(MAX_EXTRACTED_FEATURES);
    if (__orb == nullptr) ERR_MSG("Cannot allocate memory for orb", 1);
    // __orb->blurForDescriptor = true; /*should be set to true but the application crash on jetson*/
#else
    __matcher = cv::cuda::DescriptorMatcher::createBFMatcher(cv::NORM_HAMMING);

    //detector_GPU = cv::cuda::ORB::create(MAX_EXTRACTED_FEATURES); //For GPU use 
    //extractor_GPU = cv::cuda::ORB::create(); //For GPU use
    
    ORB_GPU = cv::cuda::ORB::create(MAX_EXTRACTED_FEATURES); //For GPU use 
    ORB_CPU = cv::ORB::create(MAX_EXTRACTED_FEATURES);
//    ORB_CPU = cv::AKAZE::create();
    
#endif
}

Features::~Features() {
#if (CV_MAJOR_VERSION == 2)
    delete __orb;
#else
    delete __matcher;
    delete ORB_CPU;
    delete ORB_GPU;
#endif

}

/** 
 *   @brief  Find best matches between two sets of keypoints using their descriptors.  
 *  
 *   @details The method works on GPU for both openCV 2.4 and 3.1 .
 *            The matcher type is bruteForce with crossCheck algorthim.
 *
 *   @param  in_gpu_descriptor_1 is an initialized keypoints' descriptors of GPU Mat type of first image(in openCV,it's often called query image)
 *   @param  in_gpu_descriptor_2 is an initialized keypoints' descriptors of GPU Mat type of second image(in openCV,it's often called train image)
 *   @return out_good_matches is an in initialized cv::DMatch vector which hold information about the associations
 *                            of two keypoints' sets of two giving descriptors.
 *   @return void
 */

void Features::matchTwoFrames_gpu(cv_gpu_mat &in_gpu_descriptor_1,
        cv_gpu_mat &in_gpu_descriptor_2,
        std::vector<cv::DMatch> &out_good_matches) {
    std::vector<cv::DMatch> matches12, matches21;
    
  
    
#if (CV_MAJOR_VERSION == 2)
    __matcher.match(in_gpu_descriptor_1, in_gpu_descriptor_2, matches12);
    __matcher.match(in_gpu_descriptor_2, in_gpu_descriptor_1, matches21);
#elif (CV_MAJOR_VERSION == 3)
    __matcher->match(in_gpu_descriptor_1, in_gpu_descriptor_2, matches12);
    __matcher->match(in_gpu_descriptor_2, in_gpu_descriptor_1, matches21);
#endif

    cv::DMatch forward;
    cv::DMatch backward;
    for (size_t i = 0; i < matches12.size(); i++) {
        forward = matches12[i];
        backward = matches21[forward.trainIdx];

        if ((backward.trainIdx == forward.queryIdx) && forward.distance < 10)
            out_good_matches.push_back(forward);
    }
       

}

/** 
 *   @brief  Find best matches between two sets of keypoints using their descriptors.  
 *  
 *   @details The method works on CPU for both openCV 2.4 and 3.1 .
 *            The matcher type is bruteForce with crossCheck algorthim.
 *
 *   @param  in_cpu_descriptor_1 is an initialized keypoints' descriptors of cv::Mat type of first image(in openCV,it's often called query image)
 *   @param  in_cpu_descriptor_2 is an initialized keypoints' descriptors of cv::Mat type of second image(in openCV,it's often called train image)
 *   @return out_good_matches is an in initialized cv::DMatch vector which hold information about the associations
 *                            of two keypoints' sets of two giving descriptors.
 *   @return void
 */
void Features::matchTwoFrames_cpu(cv::Mat& in_cpu_descriptor_1,
        cv::Mat& in_cpu_descriptor_2,
        std::vector<cv::DMatch> &out_good_matches) {
    std::vector<cv::DMatch> matches12, matches21;
#if (CV_MAJOR_VERSION == 2)
    // opencv 2.4 goes here
#elif (CV_MAJOR_VERSION == 3)
    // opencv 3.1 goes here
#endif

    cv::DMatch forward;
    cv::DMatch backward;
    for (size_t i = 0; i < matches12.size(); i++) {
        forward = matches12[i];
        backward = matches21[forward.trainIdx];

        if ((backward.trainIdx == forward.queryIdx) && forward.distance < 10)
            out_good_matches.push_back(forward);
    }

}

/** 
 *   @brief  Retrieve features from a giving image up to value defined in MAX_EXTRACTED_FEATURES macro 
 *  
 *   @details The method works on CPU for both openCV 2.4 and 3.1.
 *
 *   @param  in_cpu_frame is an initialized cv::Mat in grayscale.
 *   @return out_cpu_keypoints is an initialized keypoints vector extracted from in_cpu_frame.
 *   @return out_cpu_descriptors is an in initialized cv::Mat descriptors of out_cpu_keypoints.
 *   @return void
 */
void Features::getFeatures_cpu(cv::Mat &in_cpu_frame,
        std::vector<cv::KeyPoint> &out_cpu_keypoints,
        cv::Mat& out_cpu_descriptors,cv::InputArray& mask) {

#if (CV_MAJOR_VERSION == 2)
    cv::ORB extractor;
    cv::ORB detector(MAX_EXTRACTED_FEATURES);
    detector.detect(in_cpu_frame, out_cpu_keypoints);
    extractor.compute(in_cpu_frame, out_cpu_keypoints, out_cpu_descriptors);
#elif (CV_MAJOR_VERSION == 3)
//    detector = cv::ORB::create(MAX_EXTRACTED_FEATURES);
//    extractor = cv::ORB::create();
    ORB_CPU->detect(in_cpu_frame, out_cpu_keypoints,mask);
    ORB_CPU->compute(in_cpu_frame, out_cpu_keypoints, out_cpu_descriptors);
#endif
}

/** 
 *   @brief  Retrieve features from a giving image up to value defined in MAX_EXTRACTED_FEATURES macro 
 *  
 *   @details The method works on CPU for both openCV 2.4 and 3.1.
 *
 *   @param  in_gpu_frame is an initialized cv::Mat in grayscale. (it must be in grayscale to work properly)
 *   @return out_gpu_keyPoints is an initialized keypoints vector extracted from in_gpu_frame.
 *   @return out_gpu_descriptors is an in initialized gpuMat descriptors of out_gpu_keypoints.
 *   @return void
 *
 *   @warning For opencv 2.4 and ZED SDK version > 1.0 , the function will hang at calling orb->operator()
 *    and the application will not be killed with SIGTERM 
 */
void Features::getFeatures_gpu(cv_gpu_mat &in_gpu_frame,
        std::vector<cv::KeyPoint> &out_gpu_keyPoints,
        cv_gpu_mat &out_gpu_descriptors) {
#if (CV_MAJOR_VERSION == 2)
    __orb->operator()(in_gpu_frame, cv::gpu::GpuMat(),
            out_gpu_keyPoints, out_gpu_descriptors);
#elif (CV_MAJOR_VERSION == 3)
    //ORB_GPU->detect(in_gpu_frame,out_gpu_keyPoints);
    //ORB_GPU->compute(in_gpu_frame,out_gpu_keyPoints,out_gpu_descriptors) ;
    ORB_GPU->detectAndComputeAsync(in_gpu_frame,cv::noArray(),
            out_gpu_keyPoints,out_gpu_descriptors,false,stream1);
#endif
}

/** 
 *   @brief  Download GPU key points to CPU keyPoints 
 *  
 *   @details The method works for both openCV 2.4 and 3.1.
 *
 *   @param  in_gpu is an initialized gpuMat of keypoints.
 *   @return out_cpu is an in initialized cv::keypoints cpu keypoints.
 *   @return void
 */
void Features::gpu2cpu_keyPoints(cv_gpu_mat &in_gpu,
        std::vector<cv::KeyPoint> &out_cpu) {

#if (CV_MAJOR_VERSION == 2)
    __orb->downloadKeyPoints(in_gpu, out_cpu);
#elif (CV_MAJOR_VERSION == 3)
    // opencv 3.1 goes here
#endif

}

/** 
 *   @brief  Download GPU key points to CPU keyPoints 
 *  
 *   @details The method works for both openCV 2.4 and 3.1.
 *
 *   @param  in_gpu is an initialized gpuMat of keypoints.
 *   @return out_cpu is an in initialized cv::keypoints cpu keypoints.
 *   @return void
 */
void Features::gpu2cpu_keyPoints(std::vector<cv::KeyPoint> &in_gpu,
        std::vector<cv::KeyPoint> &out_cpu) {

#if (CV_MAJOR_VERSION == 2)
    //This function shouldn't be used with OpenCV2
#elif (CV_MAJOR_VERSION == 3)
    out_cpu.clear() ;
    out_cpu.assign(in_gpu.begin(),in_gpu.end());
#endif

}
 