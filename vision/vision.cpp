/*
 * features.cpp
 *
 *  Created on: Oct 16, 2016
 *      Author: yahia
 */

#include "vision.h"
#include <viso_stereo.h>

using namespace sf::vision;
using namespace sl::zed;
using namespace std;

extern int WINDOW_WIDTH;
extern int WINDOW_HEIGHT;

// VisionWork class members.

VisionWork::VisionWork() {
    __firstTime = true;
    __motor1_Tick = __motor2_Tick = 0;

#if defined ENABLE_DRAWING_THE_OUTPUT_RESULT
#pragma GCC diagnostic ignored "-Wunused-result"
#ifdef __linux__
    system("exec rm -rf camera_record.avi sensor_data.dat images_seq");
#if (SAVE_IMAGE_SEQUENCE_INSTEAD_OF_RECORDING_VIDEO)
    system("exec mkdir images_seq");
#endif
#endif
#endif

#ifdef ENABLE_REPORT_LOG_FILE
#pragma GCC diagnostic ignored "-Wunused-result"
#ifdef __linux__
    system("exec rm -rf debug_log.txt");
#endif
    __reportFilePointer = fopen("./debug_log.txt", "a");
    if (__reportFilePointer == NULL) {
        ERR_MSG("Error opening debug_log.txt!\n", 1);
    }
#endif
#if defined ENABLE_DRAWING_THE_OUTPUT_RESULT && defined ENABLE_DRAWING_THE_OUTPUT_RESULT_IN_FILE
    LOG_MSG("OLD RECORD FILE AND SENSOR FILE HAVE BEEN DELETED\n");
#if (!SAVE_IMAGE_SEQUENCE_INSTEAD_OF_RECORDING_VIDEO)
    __writer = cv::VideoWriter(__fileName, CV_FOURCC('D', 'I', 'V', '3'), RECORDED_FPS, cv::Size(OUTPUT_DRAWING_WIDTH_SIZE, OUTPUT_DRAWING_HEIGHT_SIZE));
    if (!__writer.isOpened()) {
        LOG_MSG("ERROR OPENING A FILE FOR WRITING");
    }
#endif
#endif
#if defined ENABLE_DRAWING_THE_OUTPUT_RESULT_ONLINE && defined ENABLE_DRAWING_THE_OUTPUT_RESUL
    cv::namedWindow(res_currentFrameWindowName, 0);
    cv::resizeWindow(res_currentFrameWindowName, OUTPUT_DRAWING_WIDTH_SIZE, OUTPUT_DRAWING_HEIGHT_SIZE);
#endif
#ifdef SHOW_CONFIDENCE_MATRIX
    cv::namedWindow("Confidence Map", 0);
    cv::resizeWindow("Confidence Map", OUTPUT_DRAWING_WIDTH_SIZE, OUTPUT_DRAWING_HEIGHT_SIZE);
#endif
#ifdef SHOW_DISPARITY_MATRIX
    cv::namedWindow("Disparity Map", 0);
    cv::resizeWindow("Disparity Map", OUTPUT_DRAWING_WIDTH_SIZE, OUTPUT_DRAWING_HEIGHT_SIZE);
#endif
#ifdef SHOW_ONLY_DEPTH
    cv::namedWindow("Depth KPs", 0);
    cv::resizeWindow("Depth KPs", OUTPUT_DRAWING_WIDTH_SIZE, OUTPUT_DRAWING_HEIGHT_SIZE);
#endif


}

VisionWork::~VisionWork() {
#if defined ENABLE_DRAWING_MATCHING || defined ENABLE_DRAWING_THE_OUTPUT_RESULT
    cv::destroyAllWindows();
#endif
    printf("VisionWorks destructor is called !\n");
#ifdef ENABLE_REPORT_LOG_FILE
    fclose(__reportFilePointer);
#endif
}

#ifdef ENABLE_DRAWING_THE_OUTPUT_RESULT

void VisionWork::drawResult() {
    REPORT("drawResult() is called\n");
    drawing_s drawer;
    int seq_num = 0;
    std::string seq_num_str;
    sleep(2); // wait 2 seconds
    while (true) {
        //info to show
        if (drawing_queue.empty()) {
            sleep(0.1); // wait 10ms
            continue;
        }
        drawer = drawing_queue.front();
#if(!PROFESSIONAL_SHOW)
        putText(drawer.outputFrame, drawer.res_frameNumber_str,
                cv::Point(6, OUTPUT_DRAWING_HEIGHT_SIZE - 50),
                1, 1, cv::Scalar(255, 255, 255), 2);
#else

        putText(drawer.outputFrame, "Green: Valid Depth Points",
                cv::Point(6, 30),
                1, 1, cv::Scalar(255, 255, 255), 2);

        putText(drawer.outputFrame, "Red: Not Valid Depth Points",
                cv::Point(6, 50),
                1, 1, cv::Scalar(255, 255, 255), 2);

        putText(drawer.outputFrame, drawer.res_frameNumber_str,
                cv::Point(6, 80),
                1, 1, cv::Scalar(255, 255, 255), 2);


#endif
        //drawing
        if (!drawer.current_kp.empty())
            cv::drawKeypoints(drawer.outputFrame, drawer.current_kp, drawer.outputFrame, cv::Scalar(0, 0, 255));
        if (!drawer.currentkp_valid_depth.empty())
            cv::drawKeypoints(drawer.outputFrame, drawer.currentkp_valid_depth, drawer.outputFrame, cv::Scalar(0, 255, 0));

        // drawing boundaries

        cv::resize(drawer.outputFrame, drawer.outputFrame, cv::Size(OUTPUT_DRAWING_WIDTH_SIZE, OUTPUT_DRAWING_HEIGHT_SIZE));

#if(!PROFESSIONAL_SHOW)
        __upBoundary_pix.x = 0;
        __bottomBoundary_pix.x = 0;
        cv::line(drawer.outputFrame, __upBoundary_pix, cv::Point(__upBoundary_pix.x + OUTPUT_DRAWING_WIDTH_SIZE, __upBoundary_pix.y), cv::Scalar(255, 255, 0), 3);
        cv::line(drawer.outputFrame, __bottomBoundary_pix, cv::Point(__bottomBoundary_pix.x + OUTPUT_DRAWING_WIDTH_SIZE, __bottomBoundary_pix.y), cv::Scalar(255, 255, 0), 3);
#endif

#ifdef ENABLE_DRAWING_THE_OUTPUT_RESULT_ONLINE
        imshow(res_currentFrameWindowName, drawer.outputFrame);
        cv::waitKey(1);
#endif
#ifdef ENABLE_DRAWING_THE_OUTPUT_RESULT_IN_FILE
#if (SAVE_IMAGE_SEQUENCE_INSTEAD_OF_RECORDING_VIDEO)
        seq_num_str = "./images_seq/" + std::to_string(seq_num) + ".jpg";
        imwrite(seq_num_str.c_str(), drawer.outputFrame);
        ++seq_num;
#else
        __writer.write(drawer.outputFrame);
#endif
        cv::waitKey(1);
#endif
        drawing_queue.pop();
    }
}
#endif

static void printVisionConfigurations() {

    std::cout << "\n\n\n########### Vision Configurations ###########\n\n";

#if (!USE_CROSS_CHECK_MATCH)
    std::cout << "Match Technique: "
            << "Ratio Test\n";
#else
    std::cout << "Match Technique: "
            << "CrossCheck \n";
#endif

#if (USE_VISUAL_ODOMETRY)
    std::cout << "Visual Odometry: True\n";
#else
    std::cout << "Visual Odometry: False\n";
#endif

#if (SHOW_VISUAL_ODOMETRY_PATH)
    std::cout << "Viewing Visual Odometry: True\n";
#else
    std::cout << "Viewing Visual Odometry: False\n";
#endif
#if (ENABLE_FEATURES_EXTRACTION)
    std::cout << "Features Extraction: Enabled\n";
#else
    std::cout << "Features Extraction: Disabled\n";
#endif
    std::cout << "Max number of Extracted Features: " << MAX_EXTRACTED_FEATURES << "\n";
#if (USE_ORB_GPU)
    std::cout << "ORB Version: GPU\n";
#else
    std::cout << "ORB Version: CPU\n";
#endif

#if (ENABLE_FEATURES_BOTTLE_NECK)
    std::cout << "Limit Features Density: True \n";

#if (USE_UNIFORM_DISTRIBUTION_INDEX_FOR_FEATURES_DENSITY)
    std::cout << "Limit Features Density Technique: "
            << "Random based on uniform distribution\n";
#else
    std::cout << "Limit Features Density Technique: "
            << "Percentage\n";
#endif
    std::cout << "Max number of features after limitation: " << MAX_FEATURES_DENSITY_FOR_SLAM << "\n";

#else
    std::cout << "Limit Features Density: False \n";
#endif

#ifdef PRINT_SENSOR_DATA_FILE
    std::cout << "Print sensor_data.dat :"
            << "True\n";
#else
    std::cout << "Print sensor_data.dat :"
            << "False\n";
#endif

#if (ENABLE_FLOOR_FEATURES_REMOVAL)
    std::cout << "Floor Features removal: Enabled\n";
#if (FORCE_REMOVE_BOUNDARY_FEATURES)
    std::cout << "Floor Features removal calibration: forced values\n";
#else
    std::cout << "Floor Features removal calibration: self calibrated\n";
#endif
#else
    std::cout << "Floor Features removal: Disabled\n";
#endif

#ifdef ENABLE_DRAWING_THE_OUTPUT_RESULT
    std::cout << "View Results: "
            << "Enabled\n";
#ifdef ENABLE_DRAWING_THE_OUTPUT_RESULT_ONLINE
    std::cout << "Showing result online: Enabled\n";
#else
    std::cout << "Showing result online: Disabled\n";
#endif

#if (SAVE_IMAGE_SEQUENCE_INSTEAD_OF_RECORDING_VIDEO)
    std::cout << "Saving view results as : image sequences\n";
#elif defined ENABLE_DRAWING_THE_OUTPUT_RESULT && defined ENABLE_DRAWING_THE_OUTPUT_RESULT_IN_FILE && !(SAVE_IMAGE_SEQUENCE_INSTEAD_OF_RECORDING_VIDEO)
    std::cout << "Saving view results as : video , Recorded FPS:" << RECORDED_FPS << "\n";
#else
    std::cout << "Saving view results : Disabled\n";
#endif
#else
    std::cout << "View Results: "
            << "Disabled\n";
    std::cout << "Will not draw any thing or save !\n";
#endif

    std::cout << "ZED: Max distance measurment: " << ZED_FAREST_CAPTURED_DEPTH << "\n";
#if (ZED_UNIT == 0)
    std::cout << "ZED: distance unit is mm\n";
#elif (ZED_UNIT == 1)
    std::cout << "ZED: distance unit is meter\n";
#else
    std::cout << "ZED: distance unit is cm\n";
#endif
    std::cout << "ZED View: " << WINDOW_WIDTH << " x " << WINDOW_HEIGHT << "\n";

#if !(USE_VISUAL_ODOMETRY) && !defined INCLUDE_ODOMETRY
    std::cout << "Warning: NO ODOMETRY TO PRINT , output will be zeros\n";
#endif

    std::cout << "\n\n\n########### Vision Configurations ###########\n\n";
}

void VisionWork::vision_main(int argc, char **argv) {


#if (USE_VISUAL_ODOMETRY)
    std::ifstream visual_odo_file_ptr_reader;
    bool READ_VISUAL_ODOMETRY_FROM_FILE;
    if (argc > 2) {
        std::cout << "\n\nUsing provided text file as visual odometry data source !\n";
        READ_VISUAL_ODOMETRY_FROM_FILE = true;
        visual_odo_file_ptr_reader.open(argv[2]);

        if (!visual_odo_file_ptr_reader.is_open()) {
            ERR_MSG("Error opening visual odometry file!\n", 1);
        }
    } else {
        READ_VISUAL_ODOMETRY_FROM_FILE = false;
        std::cout << "Calculating Visual odometry from SVO File .\n";
    }
#endif

    // if not success, exit program
    if (__zed.initialize(argc, argv)) {
        ERR_MSG("Cannot initialize video capture", 1);
        REPORT("Cannot initialize video camera\n");
    }

    printVisionConfigurations();
    float f, cx, cy;
    __zed.getZEDCalibrationsParams(f, cx, cy);
    std::cout << "f,cx,cy = " << f << ", " << cx << ", " << cy << std::endl;

#if (USE_VISUAL_ODOMETRY)
    //init visual odmetery
    VisualOdometryStereo::parameters param;
    param.calib.f = f; // focal length in pixels
    param.calib.cu = cx; // principal point (u-coordinate) in pixels
    param.calib.cv = cy; // principal point (v-coordinate) in pixels
    param.base = 0.12; // baseline in meters

    VisualOdometryStereo viso(param);
    Matrix yaw_pose = Matrix::eye(4);
    Matrix direction_pose = Matrix::eye(4);
    cv::Mat image_left_gray;
    cv::Mat image_right_gray;
    cv::Mat image_left(WINDOW_HEIGHT, WINDOW_WIDTH, CV_8UC4, 1);
    cv::Mat image_right(WINDOW_HEIGHT, WINDOW_WIDTH, CV_8UC4, 1);
    uint8_t *left_img_data;
    uint8_t *right_img_data;
    int32_t dims[3] = {WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_WIDTH};

    visual_odm.old_tx = visual_odm.old_tz = visual_odm.old_yaw = 0.0;
    visual_odm.tx = visual_odm.tz = visual_odm.yaw = 0.0;
    float acc_tx_delta, acc_tz_delta, acc_yaw_delta;
    acc_tx_delta = acc_tz_delta = acc_yaw_delta = 0.0;

#endif

    // <editor-fold desc="Drawing window and parameters Preparation" defaultstate="collapsed">
#if defined ENABLE_DRAWING_MATCHING || defined ENABLE_WRITING_FRAMES_TO_FILE
    cv::namedWindow("OutputFrame", 0);
    cv::resizeWindow("OutputFrame", OUTPUT_DRAWING_WIDTH_SIZE, OUTPUT_DRAWING_HEIGHT_SIZE);
#endif

    // </editor-fold>

#ifdef ENABLE_REPORT_LOG_FILE
    time_t clk = time(NULL);
    fprintf(__reportFilePointer, "startVision() is called and zed is initialized @ %s\n", ctime(&clk));
#endif
#ifdef ENABLE_DRAWING_THE_OUTPUT_RESULT
    drawingThread = std::thread([this]() {
        drawResult();
    });

    drawingThread.detach();
#endif

    // call the startCalibratingEnvironmentForDeterminigBoundariesValues
#if (ENABLE_FLOOR_FEATURES_REMOVAL)
    startCalibratingEnvironmentForDeterminigBoundariesValues();
#endif

    sl::zed::Mat zed_left;
    sl::zed::Mat zed_right;
    bool emptyKeyPoints = false;
    __cpu_currentFrame = cv::Mat::ones(__zed.zed_obj->getImageSize().height, __zed.zed_obj->getImageSize().width, CV_8UC1);
    float old_time = 0;


    while (true) {
        old_time = time(0);

#ifdef ENABLE_DRAWING_THE_OUTPUT_RESULT
        res_processing_time.start();
#endif

        // <editor-fold desc="Getting a frame and extract its features" defaultstate="collapsed">
        //__frameReadSuccess = __zed.capture_frame(__cpu_currentFrame, zed_left, zed_right);
        __frameReadSuccess = !(__zed.zed_obj->sl::zed::Camera::grab(sl::zed::SENSING_MODE::STANDARD));
        // Should be placed after the frame has been captured as specified by
        // sub fusion team


        if (__frameReadSuccess == false) {
#if(ENABLE_APPLICATION_TERMINATION_IF_NO_FRAMES_AVAILABLE_FROM_CAMERA)
            ERR_MSG("Cannot read frame from input source", 2);
#else
            LOG_MSG("Cannot read frame from camera");
#endif
            REPORT("Cannot read frame from camera\n");
            continue;
        }
        REPORT("a frame has been captured\n");

        zed_left = __zed.zed_obj->retrieveImage(sl::zed::SIDE::LEFT); // for processing on its features
#if (USE_VISUAL_ODOMETRY)
        if (READ_VISUAL_ODOMETRY_FROM_FILE) {
            // read from file instead of calculating it
            visual_odo_file_ptr_reader >> visual_odm.tx >> visual_odm.tz >> visual_odm.yaw;
            //  std::cout <<  visual_odm.tx << visual_odm.tz << visual_odm.yaw;
        } else {
            //calculate visual odometry here
            memcpy(image_left.data, zed_left.data, WINDOW_WIDTH * WINDOW_HEIGHT * 4 * sizeof (uchar));
            zed_right = __zed.zed_obj->retrieveImage(sl::zed::SIDE::RIGHT);
            memcpy(image_right.data, zed_right.data, WINDOW_WIDTH * WINDOW_HEIGHT * 4 * sizeof (uchar));

            cv::cvtColor(image_left, image_left_gray, CV_BGR2GRAY);
            cv::cvtColor(image_right, image_right_gray, CV_BGR2GRAY);
            left_img_data = image_left_gray.data;
            right_img_data = image_right_gray.data;
            if (viso.process(left_img_data, right_img_data, dims)) {

                ////////////////// For Getting correct yaw /////////////////////
                yaw_pose = (viso.getMotion()) * yaw_pose;
                visual_odm.r32 = yaw_pose.val[2][1];
                visual_odm.r33 = yaw_pose.val[2][2];
                visual_odm.yaw = std::atan2(-1 * yaw_pose.val[2][0], std::sqrt((visual_odm.r32 * visual_odm.r32) + (visual_odm.r33 * visual_odm.r33)));

                if (visual_odm.yaw < 0 /* && visual_odm.r32 > 0*/ && visual_odm.r33 < 0) {
                    visual_odm.yaw = -3.14 - visual_odm.yaw;
                } else if (visual_odm.yaw > 0 /*&& visual_odm.r32 > 0*/ && visual_odm.r33 < 0) {
                    visual_odm.yaw = 3.14 - visual_odm.yaw;
                }
                //////////////// For Getting correct x,z ////////////////////////
                direction_pose = direction_pose * Matrix::inv(viso.getMotion());
                visual_odm.tx = direction_pose.val[0][3];
                visual_odm.tz = direction_pose.val[2][3];
            } else {
                REPORT("Failed to calculate the visual odometery\n");
                std::cout << "Failed to calculate the visual odometery\n";
                continue;
            }
        }
#endif

#if (USE_VISUAL_ODOMETRY)
        visual_odm.delta_tz = (visual_odm.tz - visual_odm.old_tz) + acc_tz_delta;
        visual_odm.delta_tx = (visual_odm.tx - visual_odm.old_tx) + acc_tx_delta;
        visual_odm.delta_yaw = (visual_odm.yaw - visual_odm.old_yaw) + acc_yaw_delta;
#endif
        sl::zed::slMat2cvMat(zed_left).copyTo(__cpu_currentFrame);
        __zed.ZedMat2OpencvMat(zed_left, __cpu_currentFrame);
        //pre-processing
        cv::cvtColor(__cpu_currentFrame, __cpu_currentFrame, cv::COLOR_BGR2GRAY);
        __gpu_currentFrame.upload(__cpu_currentFrame);
        //Features extraction






#if (USE_ORB_GPU)
        __cameraEye.getFeatures_gpu(__gpu_currentFrame, __gpu_current_keypoints,
                __gpu_current_descriptors);
#else
        __zed.cvMaskforDepth(__mask);
        //imshow("depth",255*__mask) ;
        cv::waitKey(10);
        __cameraEye.getFeatures_cpu(__cpu_currentFrame, __cpu_current_keypoints,
                __cpu_descriptors, __mask);
        __gpu_current_descriptors.upload(__cpu_descriptors);
#endif

#if (USE_ORB_GPU)
        if (__gpu_current_keypoints.size() <= 0)
            emptyKeyPoints = true;
        else
            emptyKeyPoints = false;
#else
        if (__cpu_current_keypoints.empty())
            emptyKeyPoints = true;
        else
            emptyKeyPoints = false;
#endif


        if (emptyKeyPoints) {
            LOG_MSG("Unable to extract key points.");
            REPORT("Unable to extract key points\n");
            // <editor-fold desc="Pushing the drawing information" defaultstate="collapsed">
#ifdef ENABLE_DRAWING_THE_OUTPUT_RESULT
            res_processing_time.end();
            dummy_drawing.processing_time_dummy_var = res_processing_time.getTime();
            __cpu_currentFrame.copyTo(dummy_drawing.outputFrame);
            //info to show
            dummy_drawing.res_frameNumber_str = std::to_string(dummy_drawing.res_frameNumber);
            // KPs
            dummy_drawing.res_no_current_kps_str = "0";
            dummy_drawing.res_no_matched_kps_str = "0";
            dummy_drawing.res_no_valid_depth_kps_str = "0";
            dummy_drawing.current_kp.clear();
            dummy_drawing.currentkp_valid_depth.clear();
            //processing Time
            dummy_drawing.res_processing_time_str = std::to_string(dummy_drawing.processing_time_dummy_var); // in ms by default

#if(!PROFESSIONAL_SHOW)
            dummy_drawing.res_frameNumber_str = "(#,#KPs(r+g),MKPs,DKPs(green),T)= " + dummy_drawing.res_frameNumber_str + "," + dummy_drawing.res_no_current_kps_str + "," + dummy_drawing.res_no_matched_kps_str + "," +
                    dummy_drawing.res_no_valid_depth_kps_str + "," + dummy_drawing.res_processing_time_str + " ms";
#else
            dummy_drawing.res_frameNumber_str = "FPS: " + std::to_string(1000 / dummy_drawing.processing_time_dummy_var);
#endif
            drawing_queue.push(dummy_drawing);
            //Frame Number
            dummy_drawing.res_frameNumber++;
            usleep(10000); // 10 ms
#endif
            // </editor-fold>

            ///////////////////////////////////////////////////////////////////////////////////
#if (USE_VISUAL_ODOMETRY)
            visual_odm.old_tz = visual_odm.tz;
            visual_odm.old_tx = visual_odm.tx;
            visual_odm.old_yaw = visual_odm.yaw;

            acc_tx_delta = visual_odm.delta_tx;
            acc_tz_delta = visual_odm.delta_tz;
            acc_yaw_delta = visual_odm.delta_yaw;
#endif
            /////////////////////////////////////////////////////////////////////////////////
            continue;
        }
        // </editor-fold>

        if (!__firstTime) {
            // <editor-fold desc="Matcing and IDs assignment" defaultstate="collapsed">
            __good_matches.clear();
            //Association between two frames
            __cameraEye.matchTwoFrames_gpu(__gpu_current_descriptors,
                    __gpu_previous_descriptors, __good_matches);
            REPORT("Matching is done");
#if (USE_ORB_GPU)
            __cameraEye.gpu2cpu_keyPoints(__gpu_current_keypoints,
                    __cpu_current_keypoints);
#endif
            fillkeyPointsList(false);
#if (ENABLE_FEATURES_BOTTLE_NECK)
            reduceFeaturesIntensityForSLAM();
#endif
            // </editor-fold>
        } else {
            __firstTime = false;
#if (USE_ORB_GPU)
            __cameraEye.gpu2cpu_keyPoints(__gpu_current_keypoints,
                    __cpu_current_keypoints);
#endif
            fillkeyPointsList(true);
#if (ENABLE_FEATURES_BOTTLE_NECK)
            reduceFeaturesIntensityForSLAM();
#endif


        }

        __gpu_current_descriptors.copyTo(__gpu_previous_descriptors);
#if (USE_ORB_GPU)
        //__gpu_current_keypoints.copyTo(__gpu_previous_keypoints);

        __gpu_previous_keypoints.empty();
        __gpu_previous_keypoints.assign(__gpu_current_keypoints.begin(),
                __gpu_current_keypoints.end());

#endif

        ////////////////////////////////////////////////////////////////////////////////
#if (USE_VISUAL_ODOMETRY)
        visual_odm.old_tz = visual_odm.tz;
        visual_odm.old_tx = visual_odm.tx;
        visual_odm.old_yaw = visual_odm.yaw;
#endif

        // <editor-fold desc="Log Data(sensorDataFile)" defaultstate="collapsed">
#ifdef PRINT_SENSOR_DATA_FILE

#if(DROP_DYNAMIC_FRAMES_IN_YAHYA_DS)

        bool dont_print = std::string(FRAMES_TO_DROP).find(std::to_string(dummy_drawing.res_frameNumber));

#else
        const bool dont_print = false;
#endif
        if (dont_print || __keyPointsList.empty()) {
            acc_tx_delta = visual_odm.delta_tx;
            acc_tz_delta = visual_odm.delta_tz;
            acc_yaw_delta = visual_odm.delta_yaw;
        } else {
            acc_tx_delta = acc_tz_delta = acc_yaw_delta = 0.0;
            generateSensorDataFile();
        }

#endif
        // </editor-fold>
        // <editor-fold desc="Pushing the drawing information" defaultstate="collapsed">
#ifdef ENABLE_DRAWING_THE_OUTPUT_RESULT
        res_processing_time.end();
        dummy_drawing.processing_time_dummy_var = res_processing_time.getTime();
        __cpu_currentFrame.copyTo(dummy_drawing.outputFrame);
        //info to show
        dummy_drawing.res_frameNumber_str = std::to_string(dummy_drawing.res_frameNumber);
        // KPs
        dummy_drawing.res_no_current_kps_str = std::to_string(__cpu_current_keypoints.size());
        dummy_drawing.res_no_matched_kps_str = std::to_string(__good_matches.size());
        dummy_drawing.res_no_valid_depth_kps_str = std::to_string(__cpu_current_keypoints_valid_depth.size());
        dummy_drawing.current_kp.assign(__cpu_current_keypoints.begin(), __cpu_current_keypoints.end());
        dummy_drawing.currentkp_valid_depth.assign(__cpu_current_keypoints_valid_depth.begin(), __cpu_current_keypoints_valid_depth.end());
        //processing Time
        dummy_drawing.res_processing_time_str = std::to_string(dummy_drawing.processing_time_dummy_var); // in ms by default
#if(!PROFESSIONAL_SHOW)
        dummy_drawing.res_frameNumber_str = "(#,#KPs(r+g),MKPs,DKPs(green),T)= " + dummy_drawing.res_frameNumber_str + "," + dummy_drawing.res_no_current_kps_str + "," + dummy_drawing.res_no_matched_kps_str + "," +
                dummy_drawing.res_no_valid_depth_kps_str + "," + dummy_drawing.res_processing_time_str + " ms";
#else
        dummy_drawing.res_frameNumber_str = "FPS: " + std::to_string(1000 / dummy_drawing.processing_time_dummy_var);
#endif
        drawing_queue.push(dummy_drawing);
        //Frame Number
        dummy_drawing.res_frameNumber++;
#endif

        // </editor-fold>


    }
}

void VisionWork::fillkeyPointsList(bool isFirstTime) {
    //Variables
    Pixel feature_pt_location;
    KeyPoint_properties feature_property;
    unsigned int old_id;
    unsigned int trainIdx;
    // clear old list
    __keyPointsList.clear();
#if defined ENABLE_DRAWING_THE_OUTPUT_RESULT || defined ALLOW_STORING_DEPTH_KPs
    __cpu_current_keypoints_valid_depth.clear();
#endif
    __zed.retrieveDepth();
    IDAssignManager tmp_manager(__idManager.getCurrentID());
    REPORT("starting filling keyPoints\n");

    for (size_t current_KP_index = 0; current_KP_index < __cpu_current_keypoints.size(); current_KP_index++) {
        // get feature depth
        __zed.get_dimensions(__cpu_current_keypoints.at(current_KP_index).pt.y, __cpu_current_keypoints.at(current_KP_index).pt.x, feature_pt_location);
        // is valid depth ?
        if (feature_pt_location._z == -1)
            continue;

#if (ENABLE_FLOOR_FEATURES_REMOVAL)
        if (__isBoundaryValid && (feature_pt_location._y > __bottomBoundary))
            continue;
        if (__isBoundaryValid && (feature_pt_location._y < __upBoundary))
            continue; // remember, it's in negative.
#endif

        // <editor-fold desc="calculate ranging,bearing and IDs" defaultstate="collapsed">
#if defined ENABLE_DRAWING_THE_OUTPUT_RESULT || defined ALLOW_STORING_DEPTH_KPs
        __cpu_current_keypoints_valid_depth.push_back(__cpu_current_keypoints.at(current_KP_index));
#endif
        // <editor-fold desc="calculate ranging and bearing" defaultstate="collapsed">
        //ranging = r ZED unit; , bearing by rad
        feature_property._range = std::sqrt(std::pow(feature_pt_location._z, 2) + std::pow(feature_pt_location._x, 2));
        /*! \brief calculate the angle of a given point
         *  \ return angle in rad (0.9599310886 rad to - 0.9599310886 rad)
         *  due to ZED Limit field of view(110 degree).
         */
        feature_property._bearing = [&feature_pt_location]() -> float {
            if (feature_pt_location._x > 0)
                return -1 * std::atan(feature_pt_location._x / feature_pt_location._z);
            else
                return std::atan((-1 * feature_pt_location._x) / feature_pt_location._z);
        }();
        //Adding xyz-axis value for a valid kmeans clustering
        feature_property._x = feature_pt_location._x;
        feature_property._y = feature_pt_location._y;
        feature_property._z = feature_pt_location._z;

        // </editor-fold>
        //assign IDs
        if (isFirstTime) {
            __idManager.assignAvailableID((unsigned int*) &current_KP_index);
            __idManager.getID((unsigned int*) &current_KP_index, (unsigned int*) &(feature_property._ID));
        } else {
            if (__idManager.is_KP_IndexMatchPreviousKP((unsigned int*) &current_KP_index, &__good_matches, (unsigned int*) &trainIdx)) {
                REPORT("old feature is found,start getting its ID\n");
                //here we index the previous ID
                if (__idManager.getID(&trainIdx, &old_id)) {
                    tmp_manager.assignCertainID(current_KP_index, old_id);
                } else {
                    tmp_manager.assignAvailableID((unsigned int*) &current_KP_index);
                }
            } else {
                tmp_manager.assignAvailableID((unsigned int*) &current_KP_index);
                REPORT("new feature is found\n");
            }
            REPORT("getting feature ID\n");
            tmp_manager.getID((unsigned int*) &current_KP_index, (unsigned int*) &(feature_property._ID));
        }
        __keyPointsList.push_back(feature_property);
        // </editor-fold>
    }
    if (!isFirstTime) {

        __idManager = tmp_manager;
    }
    // f_file.close();
    REPORT("filling keyPoints: Done\n");
}

void VisionWork::startCalibratingEnvironmentForDeterminigBoundariesValues() {
    std::cout << "starting calibrating for Floor features removal\n";

    __upBoundary = UP_BOUNDARY_LIMIT;
    __bottomBoundary = BOTTOM_BOUNDARY_LIMIT;
    std::cout << "Forced values for the boundaries\n";
    std::cout << "up: " << __upBoundary << "\n"
            << "bottom: " << __bottomBoundary << "\n";


    __isBoundaryValid = true;
}

void getRandomNumbers(int low_limit, int high_limit, int n, std::vector<int> &r_nums) {
    r_nums.clear();
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 eng(rd()); // seed the generator
    std::uniform_int_distribution<> distr(low_limit, high_limit); // define the range

    for (int i = 0; i < n; ++i)
        r_nums.push_back(distr(eng)); // generate numbers
}

void VisionWork::reduceFeaturesIntensityForSLAM() {
#if(FEATURES_BOTTLE_NECK_METHOD == 1)
    if (__keyPointsList.size() > MAX_FEATURES_DENSITY_FOR_SLAM) {
        std::vector<int> r_indecies;
        std::vector<KeyPoint_properties> temp_keyPointsList;
        getRandomNumbers(0, __keyPointsList.size() - 1, MAX_FEATURES_DENSITY_FOR_SLAM, r_indecies);
        for (int index = 0; index < r_indecies.size(); index++) {
            temp_keyPointsList.push_back(__keyPointsList.at(r_indecies.at(index)));
        }
        __keyPointsList.clear();
        __keyPointsList.assign(temp_keyPointsList.begin(), temp_keyPointsList.end());
    }
#elif(FEATURES_BOTTLE_NECK_METHOD == 2)
    if (__keyPointsList.size() > MAX_FEATURES_DENSITY_FOR_SLAM) {
        std::sort(__keyPointsList.begin(), __keyPointsList.end(),
                [](KeyPoint_properties const &a, KeyPoint_properties const &b) {
                    return a._ID < b._ID; });

        std::vector<KeyPoint_properties> temp_keyPointsList;
        for (size_t i = 0; i < MAX_FEATURES_DENSITY_FOR_SLAM; i++)
            temp_keyPointsList.push_back(__keyPointsList.at(i));

        __keyPointsList.clear();
        __keyPointsList.assign(temp_keyPointsList.begin(), temp_keyPointsList.end());
    }
#elif(FEATURES_BOTTLE_NECK_METHOD == 3)
    if (__keyPointsList.size() > MAX_FEATURES_DENSITY_FOR_SLAM) {
        std::vector<KeyPoint_properties> temp_keyPointsList;
        std::vector<float> d_vect;
        cv::Mat centers;
        cv::Mat labels;
        //std::vector<cv::Point2f> points;
        std::vector<cv::Point3f> points;

        //           auto euclidean_polar_d = [](float& r1, float& r2,float& theta1,float& theta2 )->float{
        //               return std::sqrt( (r1*r1) + (r2*r2) - 2*r1*r2*std::cos(theta1 - theta2));
        //           };
        auto dist = [](float& x1, float& x2, float& y1, float& y2, float& z1, float& z2)->float {
            return std::sqrt((x1 - x2)*(x1 - x2)+(y1 - y2)*(y1 - y2)+(z1 - z2)*(z1 - z2));
        };



        for (int i = 0; i < __keyPointsList.size(); i++) {
            //points.push_back(cv::Point2f( __keyPointsList.at(i)._range , __keyPointsList.at(i)._bearing ));
            points.push_back(cv::Point3f(__keyPointsList.at(i)._x, __keyPointsList.at(i)._y, __keyPointsList.at(i)._z));
        }

        cv::kmeans(points, MAX_FEATURES_DENSITY_FOR_SLAM, labels, cv::TermCriteria(cv::TermCriteria::EPS + cv::TermCriteria::COUNT, 10, 1.0),
                3, cv::KMEANS_PP_CENTERS, centers);

        [&centers, &temp_keyPointsList, this, dist, &d_vect]() -> void {
            for (int i = 0; i < centers.rows; i++) {
                for (int j = 0; j < __keyPointsList.size(); j++) {
                    d_vect.push_back(
                            dist(__keyPointsList.at(j)._x, centers.at<float>(i, 0),
                            __keyPointsList.at(j)._y, centers.at<float>(i, 1),
                            __keyPointsList.at(j)._z, centers.at<float>(i, 2))

                            );
                }
                auto min_result = std::min_element(std::begin(d_vect), std::end(d_vect));
                temp_keyPointsList.push_back(__keyPointsList.at(std::distance(std::begin(d_vect), min_result)));
                d_vect.clear();
            }
        }();
        __keyPointsList.clear();
        __keyPointsList.assign(temp_keyPointsList.begin(), temp_keyPointsList.end());
    }
#endif
}

#ifdef PRINT_SENSOR_DATA_FILE

void VisionWork::generateSensorDataFile() {
    if (!__keyPointsList.empty()) {
        FILE *f = fopen("./sensor_data.dat", "a");
        if (f == NULL) {
            ERR_MSG("Error opening file!\n", 1);
        }

#if defined INCLUDE_IMU_CODE && defined INCLUDE_ODOMETRY
        fprintf(f, "ODOMETRY %i %i %i %i %f\n", __motor1_Tick, __motor2_Tick, delta_left_motor, delta_right_motor, __yaw_value);
#else
#if (USE_VISUAL_ODOMETRY) && defined INCLUDE_ODOMETRY
        fprintf(f, "ODOMETRY %i %i %i %i %.12f %.12f %.12f\n", __motor1_Tick, __motor2_Tick, delta_left_motor, delta_right_motor, -1 * visual_odm.delta_tz, -1 * visual_odm.delta_tx, visual_odm.delta_yaw);
#elif defined INCLUDE_ODOMETRY
        fprintf(f, "ODOMETRY %i %i %i %i %i %i %i\n", __motor1_Tick, __motor2_Tick, delta_left_motor, delta_right_motor, 0, 0, 0);
#elif (USE_VISUAL_ODOMETRY) // only visual odometry
        fprintf(f, "ODOMETRY %i %i %i %i %.12f %.12f %.12f\n", 0, 0, 0, 0, -1 * visual_odm.delta_tz, -1 * visual_odm.delta_tx, visual_odm.delta_yaw);
        // printf("ODOMETRY %i %i %i %i %.12f %.12f %.12f\n", 0, 0, 0, 0, -1 * visual_odm.delta_tz, -1 * visual_odm.delta_tx, visual_odm.delta_yaw);
#else
        fprintf(f, "ODOMETRY 0 0 0 0 0 0 0\n");
#endif
#endif

#ifdef INCLUDE_ODOMETRY
        old_motor1_ticks = __motor1_Tick;
        old_motor2_ticks = __motor2_Tick;
#endif
        std::for_each(__keyPointsList.begin(), __keyPointsList.end(), [this, &f](KeyPoint_properties kp) {
            fprintf(f, "SENSOR %i %.12f %.12f\n", kp._ID, kp._range, kp._bearing);
        });
        fclose(f);
    }
}
#endif