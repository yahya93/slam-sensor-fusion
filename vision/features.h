/** 
 *  @file    features.h
 *  @author  Yahia Farghaly, Yahya Ahmed
 *  @date    3/25/2016
 *  @version 1.0 
 *  
 *  @brief Wrappered class to manage the extraction and matching of features on both CPU and GPU.
 *
 *  @section DESCRIPTION
 *  The features extraction algorthim here is ORB. The Matching is done using bruteforce which is suitable for
 *  binary descriptors such as ORB. Beside the matching with bruteforce, a crossCheck algorthim is used to get
 *  unique matched points -one to one- between two images.
 *  To learn about crosscheck,
 *  read <a href="linkURL">https://stackoverflow.com/questions/11181823/why-we-need-crosscheckmatching-for-feature</a> 
 * 
 *  Here, features extraction and matching is presented for both CPU and GPU of openCV 2.4 and 3.1 .
 *  The using of openCV version is defined automatically from CV_MAJOR_VERSION macro which lies inside 
 *  openCV's header file (version.hpp). While choosing between CPU or GPU is made in form of methods of
 *  functionName_[cpu | gpu].
 *
 *  The max number of extracted features is defined in MAX_EXTRACTED_FEATURES macro in vision_config.h
 *
 *  To learn about features extraction and matching algorthims,
 *  read <a href="linkURL">https://drive.google.com/file/d/0B_QegS3rUpBUTGVLbnoxb3hOaUk/view?usp=sharing</a> 
 */

#pragma once

#include "../main/system_includes.h"
#include "../config/zed_config.h"
#include "../config/config.h"
#include "../config/vision_config.h"
#include "../main/timer.h"
#include "camera.h"
#include "types_definition.h"

/**
 * 
 * To maintain a unit signature for gpu functions arguments without losing generality.
 */
#if (CV_MAJOR_VERSION == 2)
typedef cv::gpu::GpuMat cv_gpu_mat;
#elif (CV_MAJOR_VERSION == 3)
typedef cv::cuda::GpuMat cv_gpu_mat;
#endif

/**
 * The sf namespace associates all Graduation Project codebase.
 * @author Yahia Farghaly
 * @version 1.0
 */
namespace sf {
    /**
     * The vision namespace associates all the tasks required by computer vision
     * @author Yahia Farghaly
     * @version 1.0
     */
    namespace vision {

        /**
         *  @brief Wrappered class to manage the extraction and matching of features on both CPU and GPU.
         *  @note  Methods are made with [its_name]_[cpu | gpu] is intentional, the non usage of overloaded
         *         functions feature in the language is intentional for better usage of the class.
         */
        class Features {
        public:
            
            // Default constructor to initialize private variables needed for ORB.
            Features();
            // Deconstructor to delete any allocated pointers.
            ~Features();
            // Extract and Describe features of a given image on GPU.
            void getFeatures_gpu(cv_gpu_mat &in_gpu_frame,
                    std::vector<cv::KeyPoint> &out_gpu_keyPoints,
                    cv_gpu_mat &out_gpu_descriptors);
            // Extract and Describe features of a given image on CPU.
            void getFeatures_cpu(cv::Mat &in_cpu_frame,
                    std::vector<cv::KeyPoint> &out_cpu_keypoints,
                    cv::Mat& out_cpu_descriptors,cv::InputArray& mask);
            // Match Keypoints of two frames' keypoints descriptors on GPU.
            void matchTwoFrames_gpu(cv_gpu_mat &in_gpu_descriptor_1,
                    cv_gpu_mat &in_gpu_descriptor_2,
                    std::vector<cv::DMatch> &out_good_matches);
            // Match Keypoints of two frames' keypoints descriptors on CPU.
            void matchTwoFrames_cpu(cv::Mat& in_cpu_descriptor_1,
                    cv::Mat& in_cpu_descriptor_2,
                    std::vector<cv::DMatch> &out_good_matches);
            // download gpu keypoints to cpu keypoints.                        
            void gpu2cpu_keyPoints(cv_gpu_mat &in_gpu,
                    std::vector<cv::KeyPoint> &out_cpu);
            
            void gpu2cpu_keyPoints(std::vector<cv::KeyPoint> &in_gpu,
                    std::vector<cv::KeyPoint> &out_cpu);
        private:
#if (CV_MAJOR_VERSION == 2)
            cv::gpu::ORB_GPU *__orb; ///< pointer to orb gpu class
            cv::gpu::BruteForceMatcher_GPU<cv::Hamming> __matcher; ///< holds matcher object of hamming distance of gpu
#elif(CV_MAJOR_VERSION == 3)
//            cv::Ptr<cv::FeatureDetector> detector;
//            cv::Ptr<cv::DescriptorExtractor> extractor;
            cv::Ptr<cv::ORB> ORB_CPU ;
//            cv::Ptr<cv::AKAZE> ORB_CPU ;
            cv::Ptr<cv::cuda::DescriptorMatcher> __matcher ;
            cv::Ptr<cv::cuda::ORB> ORB_GPU ;
            cv::cuda::Stream stream1;
#else
#error "Undefined OpenCV Version"
#endif
        };
    }
}
