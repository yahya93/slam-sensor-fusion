/* 
 * File:   types_definition.h
 * Author: yahia
 *
 * Created on March 24, 2017, 10:14 PM
 */

#pragma once

namespace sf {
    namespace vision {
        //New defined data types

        // <editor-fold desc="Pixel" defaultstate="collapsed">

        typedef class Pixel {
        public:
            float _x, _y, _z;

            Pixel() {
                _x = _y = _z = -1;
            }

            Pixel(float x, float y, float z) : _x(x), _y(y), _z(z) {
            }

            inline bool isEqual(const Pixel &pt) {
                return ((this->_x == pt._x) &&
                        (this->_y == pt._y) &&
                        (this->_z == pt._z));
            }

            Pixel& operator=(const Pixel &pt) {
                this->_x = pt._x;
                this->_y = pt._y;
                this->_z = pt._z;
                return *this; // for chain of = ,such as a = b = c 
            }

            Pixel operator+(const Pixel &pt) {
                Pixel tmp;
                tmp._x = this->_x + pt._x;
                tmp._y = this->_y + pt._y;
                tmp._z = this->_z + pt._z;
                return tmp;
            }

            Pixel operator-(const Pixel &pt) {
                Pixel tmp;
                tmp._x = this->_x - pt._x;
                tmp._y = this->_y - pt._y;
                tmp._z = this->_z - pt._z;
                return tmp;
            }

            inline Pixel absDiff(const Pixel &pt) {
                Pixel tmp;
                tmp._x = std::abs(pt._x - this->_x);
                tmp._y = std::abs(pt._y - this->_y);
                tmp._z = std::abs(pt._z - this->_z);
                return tmp;
            }

        } Pixel;
        // </editor-fold>

        // <editor-fold desc="KeyPoint_properties" defaultstate="collapsed">

        struct KeyPoint_properties {
            float _range;
            float _bearing;
            float _x; //for kmeans clustering
            float _y; //for kmeans clustering
            float _z; //for kmeans clustering
            unsigned int _ID; //max is  4,294,967,295
#ifdef DEVELOP_FILTERING_DYNAMIC_FEATURES
            bool _isMoving; //true if dynamic feature
            Pixel _feature_location;
#endif
#if defined ENABLE_DRAWING_MOVING_FEATURES
            cv::Point feature_pt_cv;
#endif

            KeyPoint_properties() {
                _range = 0;
                _bearing = 0;
                _x=0;
                _y=0;
                _z=0;
                _ID = 0;
#ifdef DEVELOP_FILTERING_DYNAMIC_FEATURES
                _isMoving = false;
#endif
            }
        };
        // </editor-fold>
        // <editor-fold desc="KP_Manager" defaultstate="collapsed">

        typedef class KP_ID_container {
        public:
            std::vector<unsigned int> associated_IDs;

            inline void clear() {
                associated_IDs.clear();
            }

            void operator=(const KP_ID_container &D) {
                this->clear();
                this->associated_IDs.assign(D.associated_IDs.begin(),
                        D.associated_IDs.end());
            }
        } KP_Manager;
        // </editor-fold>
    }
}

