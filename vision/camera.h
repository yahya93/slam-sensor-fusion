/* 
 * File:   zedcamera.h
 * Author: yahia
 *
 * Created on March 25, 2017, 9:39 PM
 */

#pragma once

#include "../main/system_includes.h"
#include "../config/zed_config.h"
#include "../config/config.h"
#include "../config/vision_config.h"
#include "../main/timer.h"
#include "types_definition.h"
#include "features.h"

namespace sf {
    namespace vision {

        /**
         *  A ZEDCamera class. A wrapper class for ZED functionalities 
         */
        class ZEDCamera {
        private:
            /*!< Set the ZED camera parameters */
            sl::zed::InitParams __parameters;
            int width;
            int height;
            sl::zed::Mat __current_mXYZmat;
            sl::zed::Mat __current_mDepth;

        public:
            /*!< Call ZED functions */
            sl::zed::Camera* zed_obj;
            sl::zed::Mat confidenceMap;
            sl::zed::Mat disparityMap;

            /**
             * A constructor.
             * Set the Camera parameters.
             */
            ZEDCamera();

            /**
             * A destructor.
             * Delete __zed object.
             */
            ~ZEDCamera();

            /*! \brief 		initialize the ZED camera with Parameters.
             *  \details	also set the furthest distance the ZED can
             *                  see for depth estimation.
             *  \return         0 means successful initialization.
             */
            int initialize(int argc, char**argv);

            /*! \brief 		read the current frame from ZED Camera.
             *  \return         true if it read the frame.
             *  \Note		the function is made in line for faster 
             *                  execution. 
             */
            bool capture_frame(cv::Mat& left_frame, sl::zed::Mat& left, sl::zed::Mat& right) {
                bool res = 0;

#if( BLIND_SPOT_DEPTH_FAULT_CANCELLATION_METHOD == 1)
                zed_obj->setConfidenceThreshold(CONFIDENCE_THRESHOLD_VALUE);
#endif
                res = bool(zed_obj->sl::zed::Camera::grab(sl::zed::SENSING_MODE::STANDARD) == sl::zed::ERRCODE::SUCCESS);
                left = zed_obj->retrieveImage(sl::zed::SIDE::LEFT);
                right = zed_obj->retrieveImage(sl::zed::SIDE::RIGHT);
                sl::zed::slMat2cvMat(left).copyTo(left_frame);


#ifdef GET_DEPTH_FROM_DEPTH_MAT
                this->__current_mDepth = zed_obj->retrieveMeasure(sl::zed::MEASURE::DEPTH); // Get the pointer
#else
                this->__current_mXYZmat = zed_obj->retrieveMeasure(sl::zed::MEASURE::XYZ);
#endif

                return res;
            }

            inline void retrieveDepth() {

                this->__current_mXYZmat = zed_obj->retrieveMeasure(sl::zed::MEASURE::XYZ);

            }
            /*! \brief 		return a pixel element from the current frame 
             *                  captured by the ZED Camera.
             *  \Param _row 	the y,i index of a pixel in the current frame.
             *  \Param _col 	the x,j index of a pixel in the current frame.
             *  \return         a pixel element which contains the x,y 
             *                  and z of this pixel in units.
             *  \Note		The unit is defined by __parameters.unit in the
             *                  constructor of this class.
             *  \Note		Should be called after capture_frame() function.
             *  \Note           Should be called after retrieveCurrent_M_XYZ();
             */
            void get_dimensions(int _row, int _col, Pixel& pix);
            /*! \brief 		grab the current XYZ frame from ZED Camera.
             *  \Note		the function is made in line for faster 
             *                  execution. 
             *  \Note           Must be called before get_dimensions()
             */

            /*! \brief 		used when trying something new with zed
             *  \Note		ONLY FOR TESTING
             */
            void ZED_main_test();
            void getZEDCalibrationsParams(float& f, float& cx, float& cy);

            inline void getViewDim(int& w, int& h) {
                w = this->width;
                h = this->height;
            }

            inline void ZedMat2OpencvMat(sl::zed::Mat& ZedMat, cv::Mat& cvMat) {
                if (ZedMat.data_type == sl::zed::DATA_TYPE::UCHAR) {
                    memcpy(cvMat.data, ZedMat.data, ZedMat.width * ZedMat.height * ZedMat.channels * sizeof (uchar));
                } else {
                    memcpy(cvMat.data, ZedMat.data, ZedMat.width * ZedMat.height * ZedMat.channels * sizeof (float));
                }
            }
            sf::Timer t;
            
            inline void cvMaskforDepth(cv::Mat& out_mask) {
               // t.start();
                
                
                Pixel pix;                
                cv::Mat mask(zed_obj->getImageSize().height, zed_obj->getImageSize().width, CV_8UC1);
                retrieveDepth() ;
                //memcpy(depth.data, zedDepth.data, width * height * sizeof (float));
                
                for (int row = 0; row < height; row++) {
                    for (int col = 0; col < width; col++) {
                        get_dimensions(row,col,pix);    
                        if (pix._z<=0) {
                            mask.at<uchar>(row, col) = (uchar) 0;
                        } else {
                            mask.at<uchar>(row, col) = (uchar) 1;
                        }
                    }

                }
                out_mask=mask;                
                //t.end() ;
                //std::cout << "Masking time: " << t.getTime() << std::endl;
            }
            
            
            
            
            
            
            
            
        };
    }
}


