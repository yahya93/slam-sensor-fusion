### This is a template for how the sensor file should be looks like.

    ODOMETRY <MOTOR_1_Ticks> <MOTOR_2_Ticks>
    SENSOR <Feature_ID> <range> <bearing>
    SENSOR <Feature_ID> <range> <bearing>
    .
    .
    .
    SENSOR <Feature_ID> <range> <bearing>

> Don't print '<' '>' 
> every thing inside <> is a number
> range is the r from origin of camera axis in meter
> bearing is the angle from origin in rad from z axis to x
> **NOTE:** For **bearing** , rad value is negative if it's measured from z to x , positive if it's measured from z to -x 
> FileName: sensor_data.dat
> the above format is repetitive to every frame synced with  motors tick.
