# This is a sample guide to be able to modify cmake file for this repository for adding new layer or files.

**For example:** you created a directory called x_layer which contains

 1. a **src** and **include** directory or one of them.
 2. mixed files of **cpp** and **h** .
 3. cuda files.

**Case.1** 

> **include** directory contains only **.h/.hpp** files.
>  **src** directory contains only **.cpp/.c/.cxx** files.

for **include**, 
In CMakeLists.txt,search for *#user-include* and place below it.

    include_directories(${CMAKE_CURRENT_SOURCE_DIR}/x_layer/include/)


for **src**, 
In CMakeLists.txt,search for *#src files* and place below it.

    SET(X_LAYER_FOLDER x_layer/src)
and in the 

    FILE(GLOB_RECURSE SRC_FILES "${SRC_FOLDER}/*.cpp" "${MAIN_SRC_FOLDER}/*.cpp" "${JETSON_GPIO_FOLDER}/*.cpp" "${ODOMETRY_SRC_FOLDER}/*.cpp" )

add your x_layer to be

    FILE(GLOB_RECURSE SRC_FILES "${SRC_FOLDER}/*.cpp" "${MAIN_SRC_FOLDER}/*.cpp" "${JETSON_GPIO_FOLDER}/*.cpp" "${ODOMETRY_SRC_FOLDER}/*.cpp" "${X_LAYER_FOLDER}/*.cpp" )

and for sure if it's c files it can be "*.c" or just write "/**"

**Case.2** 
The same as **Case.1** , just remove the **include** and **src** directories.
to be something like this.
 

    include_directories(${CMAKE_CURRENT_SOURCE_DIR}/x_layer/)
    SET(X_LAYER_FOLDER x_layer)
    FILE(GLOB ... "${x_layer}/*.cpp" ...) 
    
   ***Note:*** the cpp files should call .h files by #include such that the compiler can see it.
   
   **Case.3**
    For cuda files it's similar to **Case.1** & **Case.2** except you edit in the FILE command under  *#for cude files* in CMakeList.txt
    
    FILE(GLOB_RECURSE SRC_CU_FOLDER "${SRC_FOLDER}/*.cu")


