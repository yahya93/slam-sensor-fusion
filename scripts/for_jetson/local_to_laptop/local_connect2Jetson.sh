#!/bin/bash
if [[ $_ != $0 ]];then
	echo "Script is being sourced"
else
	echo "Please source the script"
	echo "Help: source ./local_connect2Jetson.sh"
	exit;
fi

# script goal is adding some nice interface with jetson in more encapsulated commands.

alias jetson_ping='ping tegra-ubuntu.local'
alias jetson_ssh='ssh ubuntu@tegra-ubuntu.local'
alias jetson_ssh_nautilus='ssh -X ubuntu@tegra-ubuntu.local nautilus'
alias jetson_ssh_x='ssh -X ubuntu@tegra-ubuntu.local'
alias jetson_poweroff="ssh ubuntu@tegra-ubuntu.local  -t 'sudo shutdown -h now; exec bash -l'"
alias jetson_X_server0="gvncviewer tegra-ubuntu.local:0"
alias jetson_temp="ssh ubuntu@tegra-ubuntu.local  -t 'watch -n 1 sensors; exec bash -l'"
alias jetson_top="ssh ubuntu@tegra-ubuntu.local  -t 'top|grep sf; exec bash -l'"

function copyFileToJetson()
{
	#Usage: scp <source> <destination>
	#scp /path/to/file username@a:/path/to/destination 
	#From this machine to jetson
	scp "$1" ubuntu@tegra-ubuntu.local:"$2"
}
alias jetson_copyFile=copyFileToJetson

function copyDirectoryTojetson()
{
    rsync -avz -e 'ssh' "$1" ubuntu@tegra-ubuntu.local:"$2"
    #You can use scp -r for recursivly copying
    #but rsync is much smarter as it can resume the copying when it's broked 
}
alias jetson_copyDir=copyDirectoryTojetson
