#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "Illegal number of parameters"
    echo "specifiy (1 argument) = the name of executable file"
    echo "Help: ./kill.sh Processname"
    exit
fi
processName="$1"
ssh -X ubuntu@tegra-ubuntu.local -t "echo ubuntu | sudo -S bash -l -c 'pid=$(pidof -s $processName)';kill $pid "