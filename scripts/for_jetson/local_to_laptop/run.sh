#!/bin/bash

# The goal of this script is to execute the binary resulted from this on host
# machine without the need to go to the long path on the remote server and
# execute it.

#if [ "$#" -ne 1 ]; then
#    echo "Illegal number of parameters"
#    echo "specifiy (1 argument) = the name of executable file"
#    echo "Help: ./run.sh filename"
#    exit
#fi

cd ../../.. # to go to the main directory
executableFileName="$1"
executable_args="$2 $3"
project_path=$(pwd)
HOST_MACHINE="$(tr '[:upper:]' '[:lower:]' <<<"$(hostname)")-$(uname -s)-$(uname -m)"

remote_project_path="/home/ubuntu/.netbeans/remote/tegra-ubuntu.local/$HOST_MACHINE""$project_path""/bin/"

echo "remote project path:"
echo "$remote_project_path"

echo "login as ssh -x and executing ""$executableFileName"
# using sudo to be able to access GPIO of jetson
ssh -X ubuntu@tegra-ubuntu.local -t "cd $remote_project_path; echo ubuntu | sudo -S bash -l -c './$executableFileName $executable_args'"
