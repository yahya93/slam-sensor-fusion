#!/bin/bash

#clean the source code of the project on the jetson machine.
#it's useful as netbeans cannot discriminate if i deleted file on the host machine, it doesn't get deleted on
#the jetson,so it will cause different compilation behaviour.

cd ../..
jetson_ip="192.168.1.4"

project_path=$(pwd)
HOST_MACHINE="$(tr '[:upper:]' '[:lower:]' <<<"$(hostname)")-$(uname -s)-$(uname -m)"

remote_project_path="/home/ubuntu/.netbeans/remote/$jetson_ip/$HOST_MACHINE""$project_path""/"
echo "login as ssh -x and removing all layers files and bin directory "
ssh -X ubuntu@$jetson_ip -t "rm -rf ~/.netbeans/remote/$jetson_ip/$HOST_MACHINE$project_path/*_layer \
~/.netbeans/remote/tegra-ubuntu.local/$HOST_MACHINE$project_path/bin ; bash -l -c exit"