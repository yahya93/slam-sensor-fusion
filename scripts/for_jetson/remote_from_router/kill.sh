#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "Illegal number of parameters"
    echo "specifiy (1 argument) = the name of executable file"
    echo "Help: ./kill.sh Processname"
    exit
fi
jetson_ip="192.168.1.4"
processName="$1"
ssh -X ubuntu@$jetson_ip -t "echo ubuntu | sudo -S bash -l -c 'pid=$(pidof -s $processName)';kill $pid "