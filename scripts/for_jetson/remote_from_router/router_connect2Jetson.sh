#!/bin/bash
if [[ $_ != $0 ]];then
	echo "Script is being sourced"
else
	echo "Please source the script"
	echo "Help: source ./router_connect2Jetson.sh"
	exit;
fi

# script goal is adding some nice interface with jetson in more encapsulated commands.
# for some reasons it doesn't accept hostname of jetson,so we replace it with its ip in the network
# future work, give jetson static IP
jetson_ip="192.168.1.4"

alias jetson_ping='ping $jetson_ip'
alias jetson_ssh='ssh ubuntu@$jetson_ip'
alias jetson_ssh_nautilus='ssh -X ubuntu@$jetson_ip nautilus'
alias jetson_ssh_x='ssh -X ubuntu@$jetson_ip'
alias jetson_poweroff="ssh ubuntu@$jetson_ip  -t 'sudo shutdown -h now; exec bash -l'"
alias jetson_X_server0="gvncviewer $jetson_ip:0"
alias jetson_temp="ssh ubuntu@$jetson_ip  -t 'watch -n 1 sensors; exec bash -l'"
alias jetson_top="ssh ubuntu@$jetson_ip  -t 'top|grep sf; exec bash -l'"

function copyFileToJetson()
{
	#Usage: scp <source> <destination>
	#scp /path/to/file username@a:/path/to/destination 
	#From this machine to jetson
	scp "$1" ubuntu@$jetson_ip:"$2"
}
alias jetson_copyFile=copyFileToJetson

function copyDirectoryTojetson()
{
    rsync -avz -e 'ssh' "$1" ubuntu@$jetson_ip:"$2"
    #You can use scp -r for recursivly copying
    #but rsync is much smarter as it can resume the copying when it's broked 
}
alias jetson_copyDir=copyDirectoryTojetson
