#include <iostream>
#include <thread>
 #include <unistd.h>
#include "../../jetson_GPIO_layer/jetson_gpio.h"

using namespace sf::jetson::gpio;
using namespace std;

#define INPUT_PIN_NUM 57
#define OUTPUT_PIN_NUM 166

void set_one_for(int numOfTimes,int time_in_sec)
{
	for(int i=0; i < numOfTimes; i++)
	{
		gpio_set_value(OUTPUT_PIN_NUM,LOW);
		sleep(time_in_sec);
		gpio_set_value(OUTPUT_PIN_NUM,HIGH);
		sleep(time_in_sec);
	}
}

int main()
{
	if(gpio_export(INPUT_PIN_NUM))
	{
		cerr << "Cannot init input pin   \n";
		return -1;
	}
	if(gpio_export(OUTPUT_PIN_NUM))
	{
		cerr << "Cannot init output pin \n";
		return -1;
	}

	gpio_set_dir(INPUT_PIN_NUM,INPUT_PIN);
	gpio_set_dir(OUTPUT_PIN_NUM,OUTPUT_PIN);

	unsigned int value = 0;

	//TEST HIGH
	gpio_set_value(OUTPUT_PIN_NUM,HIGH);
	gpio_get_value(INPUT_PIN_NUM,&value);
	std::cout << "Value: " << value << std::endl;
	sleep(1);
	if (value != 1) 
		std::cout << "Wrong read value,expected 1\n";
	else 
		std::cout <<"Writting '1': Passed \n";

	//TEST LOW
	gpio_set_value(OUTPUT_PIN_NUM,LOW);
	gpio_get_value(INPUT_PIN_NUM,&value);
	std::cout << "Value: " << value << std::endl;
	sleep(1);
	if (value != 0) 
		std::cout << "Wrong read value,expected 0\n";
	else 
		std::cout <<"Writing '0': Passed \n";

	std::cout<<"################\n";
	
	//TEST GPIO event
	gpio_set_value(OUTPUT_PIN_NUM,LOW);
	int eventresult = 0;
	
	//TEST Timeout event for 100ms
	JetsonGPIO_Interrupt_handler h1;
	h1.createEvent(INPUT_PIN_NUM,JETSON_GPIO_INTERRUPT_RISING,100);
	for(int i = 0;i<3;i++)
	{
		eventresult = h1.waitForEvent();

		if(eventresult < 0) 
		{
			std::cout << "Something is wrong happened in test#" << i << std::endl;
			std::cout << "TimeOut Test: failed\n";
		}
		else if(eventresult == 0)
		{
			std::cout << "test#"<<i<<" is timeout\n";
			std::cout << "TimeOut Test: Passed\n";
		}
		else
		{
			std::cout << "rising event is occurred\n";
			std::cout << "TimeOut Test: failed\n";
		}
	}
	h1.closeEvent();
	std::cout<<"################\n";
	
	//TEST rising event for h1
	//To refresh it after its close.
	h1.createEvent(INPUT_PIN_NUM,JETSON_GPIO_INTERRUPT_RISING,1000);
	std::thread interrupt_man(set_one_for,2,5);
	for(int i=0;i<25;i++)
	{
		eventresult = h1.waitForEvent();
		if(eventresult < 0) 
		{
			std::cout << "Something is wrong happened in test#" << i << std::endl;
		}
		else if(eventresult == 0)
		{
			std::cout << "test#"<<i<<" is timeout\n";
		}
		else
		{
			std::cout << "rising event is occurred\n";
			if (i == 4 || i == 14)
				std::cout << "Passed!\n";
		}

	}

	interrupt_man.join();

	gpio_unexport(INPUT_PIN_NUM);
	gpio_unexport(OUTPUT_PIN_NUM);
	return 0;
}