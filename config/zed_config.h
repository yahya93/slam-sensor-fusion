/* 
 * File:   zed_config.h
 * Author: yahia
 *
 * Created on February 7, 2017, 8:56 AM
 */

#pragma once

#include "vision_config.h"

// Frame input source ( HAS NO EFFECT AFTER USING ARG AND TO BE REVISED !)

//#define CAMERA_SOURCE   1 // 0=> live from camera , 1=>from svo file
//#if CAMERA_SOURCE==0
//#define   ZED_LIVE_CAMERA
//#else
//#define   ZED_SVO_FILE  
//#define   ZED_SVO_FILE_PATH "/home/ubuntu/Documents/ZED/test.svo"
//#endif 

/*! 
 * ZED Original axis relies on the middle of the left lens
 * the distance between two lenses of ZED is 12 cm = 120 mm = 0.12m
 * the middle point of the ZED in this case is 6 cm = 60mm = 0.06m
 * Setting the ZED_X_AXIS_OFFEST,ZED_Z_AXIS_OFFEST to a value will 
 * shift the ZED AXIS to another place in the world view.
 * The value you set should be consider to the unit which ZED measure space.
 * (i.e m,mm,cm,inch,...etc) 
 * It's like the new origin you want to set
 * O'(ZED_X_AXIS_OFFEST,ZED_Z_AXIS_OFFEST)
 * leave ZED_Y_AXIS_OFFEST 0 unless it's necessary
 */
#define     ZED_X_AXIS_OFFEST             0.06
#define     ZED_Z_AXIS_OFFEST             0
#define     ZED_Y_AXIS_OFFEST             0
/*!
 * ZED_QUALITY defines the quality of the image which ZED captures.
 * 0 means VGA Quality = 672*376,
 * else means HD720 = 1280*720
 */
#define     ZED_QUALITY                   0
/*!
 * ZED_UNIT defines the measured value unit of pixel captured by ZED.
 * This for x,y,z values of ZED.
 * 0 means mm
 * 1 means m
 * 2 means cm
 */
#define     ZED_UNIT                      1
/*! ZED_FAREST_CAPTURED_DEPTH defines how far can ZED calculate depth calculations
 * if it's set to 5 and ZED_UNIT is set to 1, this means that any thing beyond 5m,
 * the ZED camera will not able to calculate its depth.
 * the depth is limited to 20 meter by the ZED manufacturers.
 * ZED_FAREST_CAPTURED_DEPTH is also measured by the unit of ZED_UNIT 
 * if ZED_UNIT = 0 when ZED_FAREST_CAPTURED_DEPTH = 5 ,this means that any thing beyond 
 * 5mm,its will not measured ! and with the fact of default blind of 50 cm,it will
 * not able to calculate any thing !
 */
#define     ZED_FAREST_CAPTURED_DEPTH     10

/*!
 * Sets a filtering value for the disparity map (and by extension the depth map).
 * a value in [1,100]. A lower value means more confidence and precision (but less density),
 * an upper value reduces the filtering (more density, less certainty). Other value means no filtering.
 */
#define     CONFIDENCE_THRESHOLD_VALUE    100
/*! ZED_MODE
 * 0 => performance 
 * 1 => Quality
 */
#define     ZED_MODE    1

/*! TO Get depth from depth mat not the XYZ
 * No difference
 */
//#define GET_DEPTH_FROM_DEPTH_MAT

// 1 means,using ZED confidence threshold value which exist in ZED_CONFIG.h
// 2 means use yahia farghaly cooking way
#define BLIND_SPOT_DEPTH_FAULT_CANCELLATION_METHOD 1

#if (BLIND_SPOT_DEPTH_FAULT_CANCELLATION_METHOD == 2)
#define CALCULATE_CONFIDENCE
#define CALCULATE_DISPARITY
#endif
//#define SHOW_CONFIDENCE_MATRIX
//#define SHOW_DISPARITY_MATRIX

