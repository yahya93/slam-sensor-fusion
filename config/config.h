/* 
 * File:   config.h
 * Author: yahia
 *
 * Created on February 22, 2017, 10:19 AM
 */

#pragma once

// miscellaneous configurations
/*! \def ENABLE_REPORT_LOG_FILE
 * Active capability of recording text data during execution time in text file
 */
//#define     ENABLE_REPORT_LOG_FILE
#define     DEBUG_MODE
#define     LOG_MSG_MODE
#define     ERROR_MSG_MODE


#ifdef _MSC_VER
#define __func__ __FUNCTION__
#endif

#ifdef DEBUG_MODE
#define DBG(TXT) std::cout << __FILE__ << ":" << __LINE__ << ":" << __func__ << std::endl << TXT << "\n ";
#endif

#ifdef LOG_MSG_MODE
#define  LOG_MSG(TXT)  { time_t clk = time(NULL);     \
                    printf("%s @ %s",TXT,ctime(&clk)); \
                  };
#define PRINT(TXT) std::cout << TXT;
#define PRINT_NUM(NUM) std::cout<<std::to_string(NUM);
#endif

//It exits the code
#ifdef ERROR_MSG_MODE
#define ERR_MSG(TXT,CODE_ERR){                              \
    std::cerr<< "ERROR at: "  <<  __FILE__  << "\n";        \
    std::cerr<<"In Line: "    <<  __LINE__  << "\n";        \
    std::cerr<<"At Function: "<<  __func__  << "\n";        \
    std::cerr<<"ERR_MSG:"     <<    TXT     << "\n";        \
    time_t clk = time(NULL);                                \
    printf("At Time: %s",ctime(&clk));                      \
    printf("Code Error: %i\n",CODE_ERR);                    \
    throw std::exception(); /*for proper clean up objects*/ \
  };
#endif

// Layer config
//#define INCLUDE_IMU_CODE
//#define INCLUDE_UART
//#define INCLUDE_ODOMETRY
#define DISABLE_FEATURES_WORK 0 // Doesn't do any thing regardless value (not coded yet)