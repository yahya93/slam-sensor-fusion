 /** 
 *  @file    vision_config.h
 *  @author  Yahia Farghaly
 *  @date    10/17/2016
 *  @version 1.0 
 *  
 *  @brief Bunch of macros to control internal works inside the vision code.
 *
 *  @section DESCRIPTION
 *  The file contains only macros to enable/disable sections of the vision code or supply certain parameters
 *  values.
 *
 */

#pragma once

////////////////////// features.h configuration /////////////////////
#define     USE_ORB_GPU     0  // 1=> ORB_GPU , 0=> ORB_CPU

#if(USE_ORB_GPU)
    #define     MAX_EXTRACTED_FEATURES  2000
#else
    #define     MAX_EXTRACTED_FEATURES  500
#endif

////////////////////////////////////////////////////////////////////

////////////////////// Floor features removal configuration /////////////////////
#define     ENABLE_FLOOR_FEATURES_REMOVAL  1  // you didn't modify its code to cpu version in auto calib
#define     FORCE_REMOVE_BOUNDARY_FEATURES 1 /* 0 => software will calibrate it, 1=> force the value of boundaries by your hand.(and not able to draw it)*/
#define     UP_BOUNDARY_LIMIT       -4        //in unit of ZED(negative value) , any thing above, will be neglected
#define     BOTTOM_BOUNDARY_LIMIT 1.2       //in unit of ZED , any thing below it,will be neglected

////////////////////////////////////////////////////////////////////

#define     ENABLE_FEATURES_BOTTLE_NECK         1          // 1=> Enable reducing number of features
#define     MAX_FEATURES_DENSITY_FOR_SLAM       5         // Filtered out of the MAX_EXTRACTED_FEATURES

#define     FEATURES_BOTTLE_NECK_METHOD         3           // 1=> Random selection of features ,
                                                            // 2=> Select most associated points by  MAX_EXTRACTED_FEATURES
                                                            // 3=> kmean




#define     USE_VISUAL_ODOMETRY         1
#define     SHOW_VISUAL_ODOMETRY_PATH   0       // deprecated , doesn't do anything

/*PRINT ON CONSOLE/TERMINAL*/

/*Others*/
#define     PRINT_SENSOR_DATA_FILE
#define     MAX_ID_NUMBER           1000000

/*! \brief active a section of code which should contain all the output drawing windows in a more
 * informative way
 * IN: VisionWork::startVision()
 * the 3 following #defines are related to each other.
 */
#define     ENABLE_DRAWING_THE_OUTPUT_RESULT
#define     ENABLE_DRAWING_THE_OUTPUT_RESULT_IN_FILE
#define     ENABLE_DRAWING_THE_OUTPUT_RESULT_ONLINE
#define     PROFESSIONAL_SHOW 1
#define     OUTPUT_DRAWING_WIDTH_SIZE       672
#define     OUTPUT_DRAWING_HEIGHT_SIZE      376

#define  RECORDED_FPS   10           /*recoreded video file fps , > 0*/
#define  SAVE_IMAGE_SEQUENCE_INSTEAD_OF_RECORDING_VIDEO 0 /* Cancel recording frames into video file and 
save it as image sequences. set to 0 if u don't */

#if(SAVE_IMAGE_SEQUENCE_INSTEAD_OF_RECORDING_VIDEO)
#define     ENABLE_DRAWING_THE_OUTPUT_RESULT
#define     ENABLE_DRAWING_THE_OUTPUT_RESULT_IN_FILE
#endif

#define ENABLE_FEATURES_EXTRACTION  1 // has no effect if you change the value
#define ENABLE_APPLICATION_TERMINATION_IF_NO_FRAMES_AVAILABLE_FROM_CAMERA   1
#define DROP_DYNAMIC_FRAMES_IN_YAHYA_DS 0 // 0=> cancel , 1=> active // for VGA Quality
#define FRAMES_TO_DROP "1,2,3,4,5,6,7,8,9"
