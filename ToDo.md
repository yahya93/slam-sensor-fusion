### ToDo:
- [x] output sensor_data.dat file with the defined format (see template in the guidelines directory).
- [x] make vision work on a thread and the odmetery tick on another thread and join them in the same time.
- [x] calculate the mean and the standard deviation curve of the camera.
[SD reference](https://www.khanacademy.org/math/probability/data-distributions-a1/summarizing-spread-distributions/a/calculating-standard-deviation-step-by-step) (working on it)
- [x] write clean code for odometry_layer.
- [ ] write test files for layers.
- [ ] add the support for building the code for different layer development.
- [ ] make a layer called "arch" and add jetson dependent files inside it. as if the code will work
on another platform, we just write code inside arch with a standard interface.
- [ ] add how to use netbeans,ssh,...etc guidelines.
- [x] move origin of the camera to the middle.
- [ ] remove dynamic features.
- [x] modify angle calculations
